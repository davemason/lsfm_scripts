// @File(label = "Input file") path
// @Boolean(label = "Debug Mode?",description="If checked, will show all images (slows run speed) and print more output to the log.",value=false,persist=false) debugMode

// --------------------------------------------------
// DOWN-SAMPLE LIGHTSHEET DATA
// --------------------------------------------------
//
// Macro to reduce the size of CZI lightsheet data by loading in one timepoint at a time and 
// downsampling by decreasing bitdepth and binning.
//
// User is asked for a file, then given a dialog to select the range to process (this way you
// can use multiple machines) and how you want to downsample. Default is 8bit and 2x bin.
//
// A subdirectory of the file's directory is made (called "processed") and one tif-stack
// per timepoint is saved in that folder.
//
// A log window shows dimension information as well as the final physical size which you
// will need to enter in post-processing software.
//
//	Written and Tested in Fiji [http://fiji.sc] 64bit.
//
//												
//										Dave Mason [dnmason@liv.ac.uk] 
//										Centre for Cell Imaging [cci.liv.ac.uk]
//										University of Liverpool
//										
//
//---------------------------------------------------


//---------------------------------------------------
//-- PART 1: Open a file
//---------------------------------------------------
if (debugMode==0) {setBatchMode(true);}

dir = File.getParent(path);
name = File.getName(path);
title = File.nameWithoutExtension;

//-- Get dimension info on selected file
run("Bio-Formats Macro Extensions");
Ext.setId(path);

//-- Only process the first series
  Ext.setSeries(0);
  Ext.getSizeX(sizeX);
  Ext.getSizeY(sizeY);
  Ext.getSizeZ(sizeZ);
  Ext.getSizeC(sizeC);
  Ext.getSizeT(sizeT);
  Ext.getDimensionOrder(dimOrder);
  
//-- Print out dimensions
  print("- - - - - - - - - - - - - - - - - - - - - -");
  print("Input : "+path);
  print("- - - - - - - - - - - - - - - - - - - - - -");
  print("Image dimensions: " + sizeX + " x " + sizeY);
  print("Focal plane count = " + sizeZ);
  print("Channel count = " + sizeC);
  print("Time point count = " + sizeT);
  print("- - - - - - - - - - - - - - - - - - - - - -");

//-- check for the existance of the output directory and make it if necessary
outDir=dir+File.separator+"processed";
outDirNum=1;
outDir2=outDir;
while (File.exists(outDir2)) {
outDir2=outDir+IJ.pad(outDirNum,2);
outDirNum++;
}
outDir=outDir2;
print("Output Directory "+outDir+" does not exist. Creating...");
File.makeDirectory(outDir);
  	  
//---------------------------------------------------
//-- PART 2: Request Processing options
//---------------------------------------------------

Dialog.create("Processing options");
  Dialog.addNumber("Start Timepoint", 1);
  Dialog.addNumber("End Timepoint", sizeT);
  Dialog.addCheckbox("Crop", false);
  Dialog.addNumber("size in X", sizeX);
  Dialog.addNumber("size in Y", sizeY);
  Dialog.addNumber("X offset", 0);
  Dialog.addNumber("Y offset", 0);
  Dialog.addCheckbox("8-bit?", true);
  Dialog.addChoice("Bin Factor", newArray("1","2","4"),"2");
  if (sizeC>1){
  	Dialog.addMessage("Select Channels to include:");
  	for (i=0;i<sizeC;i++){
  Dialog.addCheckbox("Ch"+(i+1), true);
  //Dialog.addToSameRow(); //-- may have to sort this out if it can't be added on last control
  	}
  }
  Dialog.show();
  startT=Dialog.getNumber();
  endT=Dialog.getNumber();
  doCrop=Dialog.getCheckbox();
  Xdim=Dialog.getNumber();
  Ydim=Dialog.getNumber();
  Xoff=Dialog.getNumber();
  Yoff=Dialog.getNumber();
  downsample=Dialog.getCheckbox();
  binFactor=Dialog.getChoice();

//-- pull the booleans for channel retention
if (sizeC>1){
	keepChan=newArray(sizeC);
	for (i=0;i<sizeC;i++){
		keepChan[i]=Dialog.getCheckbox();
  	}
  }

  //-- Report Processing options to log
  if (downsample==true){
  print("Downsampling to 8-bit");
  }
  if (binFactor>1){
  print("Binning with Factor "+binFactor);
  }
  if (sizeC>1){
  for (i=0;i<sizeC;i++){
	print("Keeping Channel "+(i+1)+" result = "+keepChan[i]);
  	}
  }
  print("- - - - - - - - - - - - - - - - - - - - - -");

//---------------------------------------------------
//-- PART 3: Start processing the data
//---------------------------------------------------

//-- For each timepoint open a single timepoint and process. This prevents memory overflow on most systems.
for (i=startT; i<=endT; i++){
print("Processing Timepoint "+i);
run("Bio-Formats Importer", "open=["+path+"] color_mode=Default specify_range view=Hyperstack stack_order=XYCZT z_begin=1 z_end="+sizeZ+" z_step=1 t_begin="+i+" t_end="+i+" t_step=1");
if (i==startT){
	//-- On the first image, get the XYZ calibration
	getVoxelSize(width, height, depth, unit);
 	}

//-- Parse the selected channels into a string for "arrange channels"
chString="";
for (j=0;j<sizeC;j++){
	if (keepChan[j]==true){chString=chString+(j+1);}
}

//-- If no channels are selected use First one
if (chString==""){
	chString="1";
	if (debugMode==1){print("No channels selected! Using first channel");}
}
if (debugMode==1){print("Found "+sizeC+" channels. Using Channel String: "+chString);}

//-- may want to check to see if there is any change (IE don't bother running Arrange)
run("Arrange Channels...", "new="+chString);

//-- Depending upon the choices made, choose how to process
if (downsample==true){
	//-- Without resetting the levels, the resultant 8-bit image may be saturated
	//-- However if your input has very low levels of intensity, this may lead to a poor dynamic range
	//-- Ideally set this manually to the range present in your whole dataset
	setMinAndMax(0, 65535);
	run("8-bit");
}
if (binFactor>1){
run("Bin...", "x="+binFactor+" y="+binFactor+" z=1 bin=Average");
}
if (doCrop==true){
makeRectangle(Xoff, Yoff, Xdim, Ydim);
run("Crop");
}

//-- Save timepoint
saveAs("Tiff", outDir+File.separator+title+"_t"+IJ.pad(i,3)+".tif");
}
print("- - - - - - - - - - - - - - - - - - - - - -");
print("Original Pixel Size: " + width + " x " + height + " x " + depth + " ("+unit+")");
print("Processed Pixel Size: " + width*binFactor + " x " + height*binFactor + " x " + depth + " ("+unit+")");
print("- - - - - - - - - - - - - - - - - - - - - -");
print("Finished");
print("- - - - - - - - - - - - - - - - - - - - - -");