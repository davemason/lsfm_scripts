%% POST-PROCESSING SCRIPT FOR TRACKED LIGHTSHEET DATA - PLOT FIGURES
%
% Script to plot figures from post-processed lightsheet data
%
%
%                                           Written By Dave Mason [dnmason@liv.ac.uk]
%   										Centre for Cell Imaging [cci.liv.ac.uk]
%   										University of Liverpool
%
%--------------------------------------------------------------------------------------------------

%%
%---------------------------------------------------------------
% -- (1) INITIALISE AND LOAD PROCESSED DATA
%---------------------------------------------------------------
disp(['[' datestr(now,31),'] ---------- ',mfilename,' ----------']);
disp(['[' datestr(now,31),'] Plotting Figures'])

%-- Tidy up workspace before import
close all
%clearvars

%-- Get the mat file produced with processData.m
[inFile_processed,inFileDir_processed]=uigetfile('../*_processed.mat');

%-- check to make sure the dialog has not been cancelled
if inFile_processed==0
    disp(['[' datestr(now,31),'] ERROR! No data file provided']);
    return
end

disp(['[' datestr(now,31),']     Using: ',inFileDir_processed,inFile_processed]);

%-- load the data
load([inFileDir_processed,inFile_processed]);

%- Store the warning state, then turn off all warnings
w=warning;
warning('off','all');

%%
%---------------------------------------------------------------
% -- (2) SET GLOBAL PARAMETERS
%---------------------------------------------------------------

%-- PLOTTING OPTIONS ---------------------------------------------------------------
plotVars.axisFontSize=16;            %-- Size of Font used in Axis labels. Default = 12, 16-18 good for figures, anything bigger will not fit titles.
plotVars.axisLineWidth=2;            %-- Axis line width. Default = 0.5
plotVars.plotLineThickness=2;        %-- Default = 0.5
plotVars.plotLineThicknessHisto=1.5; %-- Default = 0.5 (You might want a thinner line for histograms cf CDF graphs)
plotVars.plotMarkerSize=2;           %-- Default = 6
plotVars.sameAxes=1;                 %-- [0/1] Use consistent axis scaling [1] or autoscale [0]?


%-- OUTPUT OPTIONS -----------------------------------------------------------------
plotOpts.whichTracks=0;       %-- [0/1/2] Whether to plot:
                                % [0] all tracks
                                % [1] only INNIES (no feature goes beyond boundary+20)
                                % [2] only OUTIES (any single feature goes beyond boundary+20)
plotOpts.bigSpheroids=0;     %-- [0/1] Determines how the SpotSpeed/SpotStraightness by Distance bins are calculated. 
                                % [0]= Use 2 bins for distance graphs (in/out)
                                % [1]= Use 6 bins for distance (3 in / 3 out)
plotOpts.normMax=0;          %-- [0/1] How to normalise the graphs of parameter by time/distance
                                % [0]= Normalise by area under curve
                                % [1]= Normalise by maximum value of 'Y'
plotOpts.minSpots=1000;      %-- Need at least this many Spots to plot a line graph - prevents crappy-looking normalised histograms
plotOpts.saveFIG=0;          %-- [0/1] Save a matlab FIG file? (NOTE: if neither saveFIG nor savePNG are used, graphs will stay open)
plotOpts.savePNG=0;          %-- [0/1] Save a PNG file? (see note above)
plotOpts.saveTracks=0;       %-- [0/1] Save plots of radial distance over time for migrant tracks? WARNING: can produce a lot of files!
plotOpts.addGraphTitle=0;    %-- [0/1] Add a title to the graphs (IE "Spot Speed by Radial Distance" &c.)
plotOpts.addFileNameTitle=0; %-- [0/1] Do you want to add a FileName title to each plot?
                                % Recommended to TURN OFF for fig files as it changes the axis layout
                                

%-- TRIM DATASET AS REQUIRED BY plotOpts.whichTracks   ------------------------------
if plotOpts.whichTracks==0 
    %-- plot everything
    data_sPos_plot=data_sPos;
else
%-- not all data so subset sPos based on INNIES/OUTIES
tmp_boundaryPlus=20;
%-- First get a list of all unique trackIDs that have _any_ point outside boundary + a bit
tmp_data_TrackDetails_Out=sortrows(unique(data_sPos(data_sPos(:,8)>boundary+tmp_boundaryPlus,5)));
%-- Logical Array of outies
tmp_keepArray=ismember(data_sPos(:,5),tmp_data_TrackDetails_Out);

%-- now trim data_sPos based on just innies or outies
    if plotOpts.whichTracks==1 %-- Just innies
        data_sPos_plot=data_sPos(~tmp_keepArray,:);
    end
    if plotOpts.whichTracks==2 %-- Just outies
        data_sPos_plot=data_sPos(tmp_keepArray,:);
    end
end %-- only want to plot innies or outies

clearvars tmp_data_TrackDetails_Out tmp_boundaryPlus tmp_keepArray

%%
%---------------------------------------------------------------
% -- (3) CUMLULATIVE DISTRIBUTION BY DISTANCE OVER TIME - CURVES
%---------------------------------------------------------------
                             
tmp_f=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

hold on;
for j=2:length(data_CDF(1,:))
plot(data_CDF(:,1),data_CDF(:,j),'LineWidth',plotVars.plotLineThickness)
end
xlabel('Radial Distance (um)');
ylabel('Cumulative Number of Features');

if plotVars.sameAxes==1
    xlim([0 floor(boundary*2)]);
end
%-- add a title if necessary
if plotOpts.addGraphTitle==1
    title(['Spatial Distrib^N of Features by Time']);
end
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth)

%-- Create the legend string
tmp_numPointsArray=[1 160 320 480]; %-- provide time points
tmp_legendStr = num2str(floor(tmp_numPointsArray'*3/60), 't=%2d h');
legend(tmp_legendStr,'Location','SE');

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end

if (plotOpts.saveFIG+plotOpts.savePNG)>0
    %-- save the graphs
    saveplot(tmp_f, '03_CDF');
    close(tmp_f);
end

clearvars tmp_f tmp_numPointsArray j legendStr tmp_ha

%%
%---------------------------------------------------------------
% -- (4) NORMALISED CUMLULATIVE DISTRIBUTION BY DISTANCE OVER TIME - CURVES
%---------------------------------------------------------------
tmp_f=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting
hold on

for j=2:length(data_CDFnorm(1,:))
plot(data_CDFnorm(:,1),data_CDFnorm(:,j),'LineWidth',plotVars.plotLineThickness)
end
ylim([0 1.1]);
xlabel('Radial Distance (um)');
ylabel('Norm Cumulative Number of Features');
ytickformat('%.1f')
if plotVars.sameAxes==1
    xlim([0 floor(boundary*2)]);
end
if plotOpts.addGraphTitle==1
    title(['Spatial Distrib^N of Features by Time']);
end
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth)

%-- Create the legend string
tmp_numPointsArray=[1 160 320 480]; %-- provide time points
tmp_legendStr = num2str(floor(tmp_numPointsArray'*3/60), 't=%2d h');
legend(tmp_legendStr,'Location','SE');

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end

if (plotOpts.saveFIG+plotOpts.savePNG)>0
    %-- save the graph
    saveplot(tmp_f, '04_CDF_NORM');
    close(tmp_f);
end

clearvars tmp_f tmp_numPointsArray j legendStr tmp_ha

%%
%---------------------------------------------------------------
% -- (5) SPOT SPEED BY TIME / NORMALISED SPOT SPEED BY TIME - ALL - LINE HISTOGRAM
%---------------------------------------------------------------

tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

tmp_f2=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

tmp_binNum=100; %-- How smooth should the distribution be?
tmp_numPointsArray=[1 160 320 480]; %-- provide time points

for j=1:length(tmp_numPointsArray)-1
    %-- calculate the frequency for each band between numPointsArray
    %-- Note: data_sPos(:,10) is the step/link/edge speed in um/s so multiply by 3600 to get um/h
[tmp_freq,tmp_bins]=hist(data_sPos_plot(data_sPos_plot(:,4)<tmp_numPointsArray(j+1) & data_sPos_plot(:,4)>=tmp_numPointsArray(j),10).*3600,tmp_binNum);

figure(tmp_f1);
plot(tmp_bins,tmp_freq,'LineWidth',plotVars.plotLineThicknessHisto);
hold on

        %-- depending upon plotOpts.normMax, normalise to the maximum value or the area under the curve (IE Probabilty density function)
        if plotOpts.normMax==1
            tmp_freqNorm=(tmp_freq-min(tmp_freq))/(max(tmp_freq)-min(tmp_freq));
        else
            tmp_areaUnderCurve=trapz(tmp_bins,tmp_freq);
            tmp_freqNorm=tmp_freq./tmp_areaUnderCurve;
        end

figure(tmp_f2);
plot(tmp_bins,tmp_freqNorm,'LineWidth',plotVars.plotLineThicknessHisto);
hold on

end

figure(tmp_f1);
xlabel('Windowed Spot Speed (um/h)');
if plotVars.sameAxes==1
    xlim([0 150]);
end
ylabel('Frequency');
if plotOpts.addGraphTitle==1
    title('Distrib^N of Spot Speed by Timepoint');
    if plotOpts.whichTracks==1
        title([tmp_f1.Children.Title.String,' (In)'])
    elseif plotOpts.whichTracks==2
        title([tmp_f1.Children.Title.String,' (Out)'])
    end
end
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
%-- Create the legend string
tmp_legendStr = num2str(floor(tmp_numPointsArray'*3/60), 't=%2d h');
legend(tmp_legendStr,'Location','SE');

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end


figure(tmp_f2);
if plotOpts.normMax==1
    ylim([0 1.1]);
end
xlabel('Windowed Spot Speed (um/h)');
if plotVars.sameAxes==1
    xlim([0 150]);
end
ylabel('Normalised Frequency');
if plotOpts.normMax==1
    ytickformat('%.1f')
else
    ytickformat('%.3f')
end
if plotOpts.addGraphTitle==1
    title('Normalised Distrib^N of Spot Speed by Timepoint');
    if plotOpts.whichTracks==1
        title([tmp_f2.Children.Title.String,' (In)'])
    elseif plotOpts.whichTracks==2
        title([tmp_f2.Children.Title.String,' (Out)'])
    end
end
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
%-- Create the legend string
tmp_legendStr = num2str(floor(tmp_numPointsArray'*3/60), 't=%2d h');
legend(tmp_legendStr,'Location','SE');

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end

if (plotOpts.saveFIG+plotOpts.savePNG)>0
    %-- save the graphs
    switch plotOpts.whichTracks
        case 0
            saveplot(tmp_f1, '05_ALL_STEPSPEED');saveplot(tmp_f2, '05_ALL_STEPSPEED_NORM');
            close(tmp_f1);close(tmp_f2);
        case 1
            saveplot(tmp_f1, '05_IN_STEPSPEED');saveplot(tmp_f2, '05_IN_STEPSPEED_NORM');
            close(tmp_f1);close(tmp_f2);
        case 2
            saveplot(tmp_f1, '05_OUT_STEPSPEED');saveplot(tmp_f2, '05_OUT_STEPSPEED_NORM');
            close(tmp_f1);close(tmp_f2);
    end
end
clearvars tmp_* j legendStr

%%
%---------------------------------------------------------------
% -- (6) SPOT SPEED BY DISTANCE - ALL - LINE HISTOGRAM
%---------------------------------------------------------------

tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting
tmp_f2=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

tmp_binNum=100; %-- How smooth should the distribution be?
tmp_numPointsArray=[0 100 200 300 400]; %-- provide spatial bins

%-- this is for big spheroids.
if plotOpts.bigSpheroids==1
    %-- Break down into 3 in and 3 out
    tmp_numPointsArray=round([0:boundary/3:boundary*2],-1);
else
    %-- Just use in / out
    tmp_numPointsArray=round([0:boundary:boundary*2],-1);
end

for j=1:length(tmp_numPointsArray)-1
    %-- calculate the frequency distribution of spot speed (column 10) as a function of radial distance. Multiply by 3600 to get speed in um/h
[tmp_freq,tmp_bins]=hist(data_sPos_plot(data_sPos_plot(:,8)<tmp_numPointsArray(j+1) & data_sPos_plot(:,8)>=tmp_numPointsArray(j),11)*3600,tmp_binNum);

    if sum(tmp_freq)>plotOpts.minSpots %-- only plot if there are enough values for a reasonable graph

        figure(tmp_f1);
        plot(tmp_bins,tmp_freq,'LineWidth',plotVars.plotLineThicknessHisto);
        hold on
        
        %-- depending upon plotOpts.normMax, normalise to the maximum value or the area under the curve (IE Probabilty density function)
        if plotOpts.normMax==1
            tmp_freqNorm=(tmp_freq-min(tmp_freq))/(max(tmp_freq)-min(tmp_freq));
        else
            tmp_areaUnderCurve=trapz(tmp_bins,tmp_freq);
            tmp_freqNorm=tmp_freq./tmp_areaUnderCurve;
        end

        figure(tmp_f2);
        plot(tmp_bins,tmp_freqNorm,'LineWidth',plotVars.plotLineThicknessHisto);
        hold on

    end

end
figure(tmp_f1);
xlabel('Windowed Spot Speed (um/h)');
if plotVars.sameAxes==1
    xlim([0 150]);
end
ylabel('Frequency');
if plotOpts.addGraphTitle==1
    title('Distrib^N of Spot Speed by Radial Distance')
	if plotOpts.whichTracks==1
        title([tmp_f1.Children.Title.String,' (In)'])
    elseif plotOpts.whichTracks==2
        title([tmp_f1.Children.Title.String,' (Out)'])
    end
end
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
legendCell = cellstr([num2str(tmp_numPointsArray', 'd=%-d-') num2str(circshift(tmp_numPointsArray,[0 -1])', ' %-d um')]);
legend(legendCell,'Location','NE');

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end

figure(tmp_f2);

if plotOpts.normMax==1
    ylim([0 1.1]);
end

xlabel('Windowed Spot Speed (um/h)');
if plotVars.sameAxes==1
    xlim([0 150]);
end
ylabel('Normalised Frequency');

if plotOpts.normMax==1
    ytickformat('%.1f')
else
    ytickformat('%.3f')
end

if plotOpts.addGraphTitle==1
    title('Normalised Distrib^N of Spot Speed by Radial Distance')
        if plotOpts.whichTracks==1
        title([tmp_f2.Children.Title.String,' (In)'])
    elseif plotOpts.whichTracks==2
        title([tmp_f2.Children.Title.String,' (Out)'])
    end
end
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);

legend(legendCell,'Location','NE');

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end

if (plotOpts.saveFIG+plotOpts.savePNG)>0
    %-- save the graphs
    switch plotOpts.whichTracks
        case 0
            saveplot(tmp_f1, '06_ALL_SPOTSPEED_BY_DIST');saveplot(tmp_f2, '06_ALL_SPOTSPEED_BY_DIST_NORM');
            close(tmp_f1);close(tmp_f2);
        case 1
            saveplot(tmp_f1, '06_IN_SPOTSPEED_BY_DIST');saveplot(tmp_f2, '06_IN_SPOTSPEED_BY_DIST_NORM');
            close(tmp_f1);close(tmp_f2);
        case 2
            saveplot(tmp_f1, '06_OUT_SPOTSPEED_BY_DIST');saveplot(tmp_f2, '06_OUT_SPOTSPEED_BY_DIST_NORM');
            close(tmp_f1);close(tmp_f2);
    end
end

clearvars tmp_* j legendStr

%%
%---------------------------------------------------------------
% -- (7) SPOT STRAIGHTNESS BY TIME - ALL - LINE HISTOGRAM
%---------------------------------------------------------------

tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting
tmp_f2=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

tmp_binNum=200; %-- How smooth should the distribution be?
tmp_numPointsArray=[1 160 320 480]; %-- provide time points

for j=1:length(tmp_numPointsArray)-1;
    %-- need to also check for zero values as straightness is a windowed parameter
[tmp_freq,tmp_bins]=hist(data_sPos_plot(data_sPos_plot(:,4)<tmp_numPointsArray(j+1) & data_sPos_plot(:,12)~=0 & data_sPos_plot(:,4)>=tmp_numPointsArray(j),12),tmp_binNum); 
figure(tmp_f1);
plot(tmp_bins,tmp_freq,'LineWidth',plotVars.plotLineThicknessHisto);
hold on

        %-- depending upon plotOpts.normMax, normalise to the maximum value or the area under the curve (IE Probabilty density function)
        if plotOpts.normMax==1
            tmp_freqNorm=(tmp_freq-min(tmp_freq))/(max(tmp_freq)-min(tmp_freq));
        else
            tmp_areaUnderCurve=trapz(tmp_bins,tmp_freq);
            tmp_freqNorm=tmp_freq./tmp_areaUnderCurve;
        end
        
figure(tmp_f2);
plot(tmp_bins,tmp_freqNorm,'LineWidth',plotVars.plotLineThicknessHisto);
hold on

end

figure(tmp_f1);
xlabel('Straightness');
xtickformat('%.1f')
if plotVars.sameAxes==1
    xlim([0 1.1]);
end
ylabel('Frequency');
if plotOpts.addGraphTitle==1
    title('Distrib^N of Spot Straightness by Timepoint');
    if plotOpts.whichTracks==1
        title([tmp_f1.Children.Title.String,' (In)'])
    elseif plotOpts.whichTracks==2
        title([tmp_f1.Children.Title.String,' (Out)'])
    end
end
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
%-- Create the legend string
tmp_legendStr = num2str(floor(tmp_numPointsArray'*3/60), 't=%2d h');
legend(tmp_legendStr,'Location','NW');

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end

figure(tmp_f2);
if plotOpts.normMax==1
    ylim([0 1.1]);
end
xlabel('Straightness');
xtickformat('%.1f')
if plotVars.sameAxes==1
    xlim([0 1.1]);
end
ylabel('Normalised Frequency');
if plotOpts.normMax==1
    ytickformat('%.1f')
else
    ytickformat('%.3f')
end
if plotOpts.addGraphTitle==1
    title('Normalised Distrib^N of Spot Straightness by Timepoint');
    if plotOpts.whichTracks==1
        title([tmp_f2.Children.Title.String,' (In)'])
    elseif plotOpts.whichTracks==2
        title([tmp_f2.Children.Title.String,' (Out)'])
    end
end
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
%-- Create the legend string
tmp_legendStr = num2str(floor(tmp_numPointsArray'*3/60), 't=%2d h');
legend(tmp_legendStr,'Location','NW');

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end

if (plotOpts.saveFIG+plotOpts.savePNG)>0
%-- save the graphs
    switch plotOpts.whichTracks
        case 0
            saveplot(tmp_f1, '07_ALL_SPOTSTRAIGHT_BY_TIME');saveplot(tmp_f2, '07_ALL_SPOTSTRAIGHT_BY_TIME_NORM');
            close(tmp_f1);close(tmp_f2);
        case 1
            saveplot(tmp_f1, '07_IN_SPOTSTRAIGHT_BY_TIME');saveplot(tmp_f2, '07_IN_SPOTSTRAIGHT_BY_TIME_NORM');
            close(tmp_f1);close(tmp_f2);
        case 2
            saveplot(tmp_f1, '07_OUT_SPOTSTRAIGHT_BY_TIME');saveplot(tmp_f2, '07_OUT_SPOTSTRAIGHT_BY_TIME_NORM');
            close(tmp_f1);close(tmp_f2);
    end
end

clearvars tmp_* j legendStr


%%
%---------------------------------------------------------------
% -- (8) SPOT STRAIGHTNESS BY DISTANCE - ALL - LINE HISTOGRAM
%---------------------------------------------------------------
tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting
tmp_f2=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

tmp_binNum=200; %-- How smooth should the distribution be?
tmp_numPointsArray=[0 100 200 300 400]; %-- provide spatial bins

%-- this is for big spheroids.
if plotOpts.bigSpheroids==1
    %-- Break down into 3 in and 3 out
    tmp_numPointsArray=round([0:boundary/3:boundary*2],-1);
else
    %-- Just use in / out
    tmp_numPointsArray=round([0:boundary:boundary*2],-1);
end

for j=1:length(tmp_numPointsArray)-1
[tmp_freq,tmp_bins]=hist(data_sPos_plot(data_sPos_plot(:,8)<tmp_numPointsArray(j+1) & data_sPos_plot(:,12)~=0 & data_sPos_plot(:,8)>=tmp_numPointsArray(j),12),tmp_binNum); 

    if sum(tmp_freq)>plotOpts.minSpots %-- only plot if there are enough values for a reasonable graph

        figure(tmp_f1);
        plot(tmp_bins,tmp_freq,'LineWidth',plotVars.plotLineThicknessHisto);
        hold on
        
        %-- depending upon plotOpts.normMax, normalise to the maximum value or the area under the curve (IE Probabilty density function)
        if plotOpts.normMax==1
            tmp_freqNorm=(tmp_freq-min(tmp_freq))/(max(tmp_freq)-min(tmp_freq));
        else
            tmp_areaUnderCurve=trapz(tmp_bins,tmp_freq);
            tmp_freqNorm=tmp_freq./tmp_areaUnderCurve;
        end

        figure(tmp_f2);
        plot(tmp_bins,tmp_freqNorm,'LineWidth',plotVars.plotLineThicknessHisto);
        hold on
    end
end
figure(tmp_f1);
xlabel('Straightness');
xtickformat('%.1f')
if plotVars.sameAxes==1
    xlim([0 1.1]);
end
ylabel('Frequency');
if plotOpts.addGraphTitle==1
    title('Distrib^N of Spot Straightness by Radial Distance')
	if plotOpts.whichTracks==1
        title([tmp_f1.Children.Title.String,' (In)'])
    elseif plotOpts.whichTracks==2
        title([tmp_f1.Children.Title.String,' (Out)'])
    end
end
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);

legendCell = cellstr([num2str(tmp_numPointsArray', 'd=%-d-') num2str(circshift(tmp_numPointsArray,[0 -1])', ' %-d um')]);
legend(legendCell,'Location','NW');

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end

figure(tmp_f2);

if plotOpts.normMax==1
    ylim([0 1.1]);
end

xlabel('Straightness');
xtickformat('%.1f')
if plotVars.sameAxes==1
    xlim([0 1.1]);
end
ylabel('Normalised Frequency');
if plotOpts.normMax==1
    ytickformat('%.1f')
else
    ytickformat('%.3f')
end
if plotOpts.addGraphTitle==1
    title('Normalised Distrib^N of Spot Straightness by Radial Distance')
    if plotOpts.whichTracks==1
        title([tmp_f2.Children.Title.String,' (In)'])
    elseif plotOpts.whichTracks==2
        title([tmp_f2.Children.Title.String,' (Out)'])
    end
end
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
legend(legendCell,'Location','NW');

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end

if (plotOpts.saveFIG+plotOpts.savePNG)>0
    %-- save the graphs
    switch plotOpts.whichTracks
        case 0
            saveplot(tmp_f1, '08_ALL_SPOTSTRAIGHT_BY_DIST');saveplot(tmp_f2, '08_ALL_SPOTSTRAIGHT_BY_DIST_NORM');
            close(tmp_f1);close(tmp_f2);
        case 1
            saveplot(tmp_f1, '08_IN_SPOTSTRAIGHT_BY_DIST');saveplot(tmp_f2, '08_IN_SPOTSTRAIGHT_BY_DIST_NORM');
            close(tmp_f1);close(tmp_f2);
        case 2
            saveplot(tmp_f1, '08_OUT_SPOTSTRAIGHT_BY_DIST');saveplot(tmp_f2, '08_OUT_SPOTSTRAIGHT_BY_DIST_NORM');
            close(tmp_f1);close(tmp_f2);
    end
end

clearvars tmp_* j legendStr legendCell


%%
%---------------------------------------------------------------
% -- (9) SPOT SPEED - ALL - LINE HISTOGRAM
%---------------------------------------------------------------

tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

tmp_binNum=100; %-- How smooth should the distribution be?

%-- calculate bins/frequencies and multiply speed by 3600 to use um/h
[tmp_freq,tmp_bins]=hist(data_sPos_plot(:,10)*3600,tmp_binNum);
plot(tmp_bins,tmp_freq,'LineWidth',plotVars.plotLineThickness); %-- because there's only one use the thicker line

xlabel('Speed (um/h)');
if plotVars.sameAxes==1
    xlim([0 150]);
end
ylabel('Frequency');
ylim([0 max(tmp_freq)*1.05]);
if plotOpts.addGraphTitle==1
    title('Distrib^N of Spot Speed')
	if plotOpts.whichTracks==1
        title([tmp_f1.Children.Title.String,' (In)'])
    elseif plotOpts.whichTracks==2
        title([tmp_f1.Children.Title.String,' (Out)'])
	end
end
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end

if (plotOpts.saveFIG+plotOpts.savePNG)>0
    %-- save the graph
    switch plotOpts.whichTracks
        case 0
            saveplot(tmp_f1, '09_ALL_SPOT_SPEED');
            close(tmp_f1);
        case 1
            saveplot(tmp_f1, '09_IN_SPOT_SPEED');
            close(tmp_f1);
        case 2
            saveplot(tmp_f1, '09_OUT_SPOT_SPEED');
            close(tmp_f1);
    end
end
clearvars tmp_*

%%
%---------------------------------------------------------------
% -- (10) SPOT STRAIGHTNESS - ALL - LINE HISTOGRAM
%---------------------------------------------------------------
tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

tmp_binNum=100; %-- How smooth should the distribution be?

[tmp_freq,tmp_bins]=hist(data_sPos_plot(data_sPos_plot(:,12) ~=0,12),tmp_binNum);
plot(tmp_bins,tmp_freq,'LineWidth',plotVars.plotLineThickness); %-- because there's only one use the thicker line

xlabel('Straightness');
xtickformat('%.1f')
if plotVars.sameAxes==1
    xlim([0 1.1]);
end
ylabel('Frequency');
ylim([0 max(tmp_freq)*1.05]);
if plotOpts.addGraphTitle==1
    title('Distrib^N of Spot Straightness')
	if plotOpts.whichTracks==1
        title([tmp_f1.Children.Title.String,' (In)'])
    elseif plotOpts.whichTracks==2
        title([tmp_f1.Children.Title.String,' (Out)'])
    end
end
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end

if (plotOpts.saveFIG+plotOpts.savePNG)>0
    %-- save the graph
    switch plotOpts.whichTracks
        case 0
            saveplot(tmp_f1, '10_ALL_SPOT_STRAIGHT');
            close(tmp_f1);
        case 1
            saveplot(tmp_f1, '10_IN_SPOT_STRAIGHT');
            close(tmp_f1);
        case 2
            saveplot(tmp_f1, '10_OUT_SPOT_STRAIGHT');
            close(tmp_f1);
    end
end
clearvars tmp_*

%%
%---------------------------------------------------------------
% -- (11) SPOT SPEED - ALL - BEEHIVE
%---------------------------------------------------------------

tmp_xrand=0.1.*randn(size(data_sPos_plot,1),1)+1;

%-- Plot the graph
tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

plot(tmp_xrand,data_sPos_plot(:,10).*3600,'.','MarkerSize',plotVars.plotMarkerSize);
xlim([0 2]);
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth,'XTick', [])

ylim([0 250]);
ylabel('Speed (um/h)');
if plotOpts.addGraphTitle==1
    title('Spot Speed','FontSize',12);
   	if plotOpts.whichTracks==1
        title([tmp_f1.Children.Title.String,' (In)'])
    elseif plotOpts.whichTracks==2
        title([tmp_f1.Children.Title.String,' (Out)'])
    end

end
hold on
plot([0.4 1.6],[nanmean(data_sPos_plot(:,10)).*3600 nanmean(data_sPos_plot(:,10)).*3600],'r-','LineWidth',1, 'HandleVisibility','off');

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
    tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end

if (plotOpts.saveFIG+plotOpts.savePNG)>0;
    %-- save the graph
    switch plotOpts.whichTracks
        case 0
            saveplot(tmp_f1, '11_ALL_SPOT_SPEED_SWARM');
            close(tmp_f1);
        case 1
            saveplot(tmp_f1, '11_IN_SPOT_SPEED_SWARM');
            close(tmp_f1);
        case 2
            saveplot(tmp_f1, '11_OUT_SPOT_SPEED_SWARM');
            close(tmp_f1);
    end
end

clearvars tmp_*

%%
%---------------------------------------------------------------
% -- (12) SPOT STRAIGHTNESS - ALL - BEEHIVE
%---------------------------------------------------------------

%-- Prepare the data for plotting (strip zeros)
tmp_yVals=data_sPos_plot(data_sPos_plot(:,12)~=0,12);
tmp_xrand=0.1.*randn(size(tmp_yVals,1),1)+1;

%-- Plot the graph
tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

plot(tmp_xrand,tmp_yVals,'.','MarkerSize',plotVars.plotMarkerSize);
xlim([0 2]);
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth,'XTick', [])

ylabel('Straightness');
ytickformat('%.1f')

if plotOpts.addGraphTitle==1
    title('Spot Straightness','FontSize',12);
   	if plotOpts.whichTracks==1
        title([tmp_f1.Children.Title.String,' (In)'])
    elseif plotOpts.whichTracks==2
        title([tmp_f1.Children.Title.String,' (Out)'])
    end
end
hold on
plot([0.4 1.6],[mean(tmp_yVals) mean(tmp_yVals)],'r-','LineWidth',1, 'HandleVisibility','off');

if plotOpts.addFileNameTitle==1
%-- Add a FileName title
set(gca, 'OuterPosition', [0 0 1 .95]);
tmp_ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
text(0.5, 1,inFile,'HorizontalAlignment','center','VerticalAlignment', 'top','interpreter','none','FontSize',12)
end


if (plotOpts.saveFIG+plotOpts.savePNG)>0;
%-- save the graph
    switch plotOpts.whichTracks
        case 0
            saveplot(tmp_f1, '12_ALL_SPOT_STRAIGHT_SWARM');
            close(tmp_f1);
        case 1
            saveplot(tmp_f1, '12_IN_SPOT_STRAIGHT_SWARM');
            close(tmp_f1);
        case 2
            saveplot(tmp_f1, '12_OUT_SPOT_STRAIGHT_SWARM');
            close(tmp_f1);
    end
end

clearvars tmp_* 

%%
%---------------------------------------------------------------
% -- (13) Plot radial distance as a function of time for tracks that cross the boundary
%---------------------------------------------------------------
%-- can produce many plots so make optional in global params
if plotOpts.saveTracks==1

%-- First trim to only those tracks with both internal and external parts (IE countIn*countOut > zero)
tmp_TrackList=data_TrackDetails_Out(data_TrackDetails_Out(:,3).*data_TrackDetails_Out(:,4)>0,:);
%-- for brevity trim to tracks over 150 frames
tmp_TrackList=tmp_TrackList(tmp_TrackList(:,2)>150,:);
    
for tmp_trackInd=1:size(tmp_TrackList,1)
    %-- hide so it is quicker to run: you can show it mid-run using [tmp_f1.Visible='on';]
    tmp_f1=figure('visible','off');
    plot(data_sPos(data_sPos(:,5)==tmp_TrackList(tmp_trackInd,1),4),data_sPos(data_sPos(:,5)==tmp_TrackList(tmp_trackInd,1),8));
    
    %-- set the minimum to zero?
    %yl = ylim(gca);
    %yl(1) = 0;
    %ylim(gca, yl);
    
    hold on
    plot([0 500],[boundary boundary],'r-')
    xlabel 'Frame';
    ylabel 'Radial Distance (um)';
    title([inFile,' (>150frames, ext+int parts) : track ' num2str(tmp_TrackList(tmp_trackInd,1)-10^9)])
    
    %-- Save out the graphs.
    %-- Consider putting a catch in here to prevent thousands of graphs being saved?
    %saveas(tmp_f1,[inFileDir_processed,replace(inFile_processed,'_processed.mat','_'),sprintf('%06.f',tmp_TrackList(tmp_trackInd,1)-10^9),'.png']);
    if (plotOpts.saveFIG+plotOpts.savePNG)>0;
    saveplot(tmp_f1, ['13_track_',sprintf('%06.f',tmp_TrackList(tmp_trackInd,1)-10^9)]);
    end

    %-- always close these plots
    close(tmp_f1)
end

clearvars tmp_*

end
%%
%---------------------------------------------------------------
% -- (14) Plot the spherical components of velocity for all tracks
%---------------------------------------------------------------
tmp_binNum=50;
tmp_f1=figure('Position', [100, 100, 1400, 500]);

[tmp_a,tmp_b]=hist(data_sPos(:,22),tmp_binNum);
tmp_a_norm=tmp_a./max(tmp_a);
subplot(1,3,1)
plot(tmp_b',tmp_a_norm','b-','LineWidth',plotVars.plotLineThicknessHisto)
%set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
hold on
title('Azimuthal')
xlabel 'Speed (um/s)'
ylabel 'Normalised Frequency'
ytickformat('%.1f')

[tmp_a,tmp_b]=hist(data_sPos(:,23),tmp_binNum);
tmp_a_norm=tmp_a./max(tmp_a);
subplot(1,3,2)
plot(tmp_b',tmp_a_norm','b-','LineWidth',plotVars.plotLineThicknessHisto)
%set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
hold on
title('Elevational')
xlabel 'Speed (um/s)'
ylabel 'Normalised Frequency'
ytickformat('%.1f')

[tmp_a,tmp_b]=hist(data_sPos(:,24),tmp_binNum);
tmp_a_norm=tmp_a./max(tmp_a);
subplot(1,3,3)
plot(tmp_b',tmp_a_norm','b-','LineWidth',plotVars.plotLineThicknessHisto)
%set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
hold on
title('Radial')
xlabel 'Speed (um/s)'
ylabel 'Normalised Frequency'
ytickformat('%.1f')

if (plotOpts.saveFIG+plotOpts.savePNG)>0;
    saveplot(tmp_f1, '14_AZ_EL_RAD_SPEED');
    close(tmp_f1);
end

clearvars tmp_*

%%
%---------------------------------------------------------------
% -- (15) Plot the radial speed as a function of radial distance
%---------------------------------------------------------------

%-- Plot radial speed by radial distance
tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

plot(data_sPos(:,21),data_sPos(:,24),'.b')
%histogram2(data_sPos(:,21),data_sPos(:,24),'DisplayStyle','tile','ShowEmptyBins','off'); %-- alternatively use a 2D histogram which gives a better idea about areas of dense spots
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
xlabel 'Spot Radial Distance (um)'
ylabel 'Spot Radial Speed (um/s)'
hold on

%-- Find the plot range
tmp_yrange = ylim(gca);
%-- Use to add an indicator of the boundary
plot([boundary,boundary],tmp_yrange,'r-')
if plotOpts.addGraphTitle==1
    title('Radial speed by distance')
end

%---------------------------------------------------------------

%-- Plot radial speed by time
tmp_f2=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

%-- pull the data (note that some of the the radial speeds are NaN which cannot be used to fit
tmp_notNaN=data_sPos(~isnan(data_sPos(:,24)),[4,24]);
plot(tmp_notNaN(:,1),tmp_notNaN(:,2),'.b')
%histogram2(tmp_notNaN(:,1),tmp_notNaN(:,2),'DisplayStyle','tile','ShowEmptyBins','off'); %-- alternatively use a 2D histogram which gives a better idea about areas of dense spots
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
xlabel 'Time (frames)'
ylabel 'Spot Radial Speed (um/s)'


if plotOpts.addGraphTitle==1
    title('Radial speed by time')
end

%-- Check whether anything needs to be saved. Will also leave the plot open if not.
if (plotOpts.saveFIG+plotOpts.savePNG)>0;
    saveplot(tmp_f1, '15_RADIALSPEED_RADIALDIST');
    saveplot(tmp_f2, '15_RADIALSPEED_TIME');
    close(tmp_f1);close(tmp_f2);
end
clearvars tmp_*
    
%%
%---------------------------------------------------------------
% -- (16) INTRA-TRACK in/out ratios for total speed
%---------------------------------------------------------------

tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 1200 500]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting
subplot(1,2,2)

%-- first exclude anything that doesn't cross the boundary (IE outies with no innies or innies with no outies)
tmp_1=data_TrackDetails_Out(~isnan(data_TrackDetails_Out(:,15).*data_TrackDetails_Out(:,16)),15:16);
%-- calculate the ratio in/out: IE >1 means faster inside
tmp_1(:,3)=log(tmp_1(:,1)./tmp_1(:,2));

%-- calculate histogram and plot
tmp_binNum=floor(sqrt(size(tmp_1,1)));
[tmp_freq,tmp_bins]=hist(tmp_1(:,3),tmp_binNum);
bar(tmp_bins,tmp_freq,'edgecolor','none')
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
%-- scale the yaxis to give the bars some space
ylim([0 floor(max(tmp_freq)*1.05)])

%-- add a line showing '1' and embelish the plot
hold on
plot([0 0],[0 max(tmp_freq)*1.05],'r-','LineWidth',plotVars.plotLineThickness)
if plotOpts.addGraphTitle==1
    title('Dist^N of total speed ratios')
end
xlabel 'Paired Ratio of inside/outside speed'
ylabel 'Frequency'

%-- add the normalised distribution of speeds for inside and outside of outies (not paired!)

%-- get a list of tracks that have both innies and outies
tmp_TrackList=data_TrackDetails_Out(data_TrackDetails_Out(:,3).*data_TrackDetails_Out(:,4)>0,1);
tmp_spotsHaveBoth=data_sPos(ismember(data_sPos(:,5),tmp_TrackList),:);
%-- now split into inside and outside spots
tmp_spotsHaveBoth_In=tmp_spotsHaveBoth(tmp_spotsHaveBoth(:,8)<boundary,:);
tmp_spotsHaveBoth_Out=tmp_spotsHaveBoth(tmp_spotsHaveBoth(:,8)>boundary,:);

%-- use the smaller of the two arrays to pick the binNum
tmp_binNum=floor( sqrt( min( [ size(tmp_spotsHaveBoth_In,1) , size(tmp_spotsHaveBoth_Out,1)] ) ) );

subplot(1,2,1)
[tmp_freq,tmp_bins]=hist(tmp_spotsHaveBoth_In(:,10),tmp_binNum);
tmp_freq=tmp_freq./max(tmp_freq);
plot(tmp_bins,tmp_freq,'-','LineWidth',plotVars.plotLineThicknessHisto);
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
%-- repeat for the outside of outies
hold on
[tmp_freq,tmp_bins]=hist(tmp_spotsHaveBoth_Out(:,10),tmp_binNum);
tmp_freq=tmp_freq./max(tmp_freq);
plot(tmp_bins,tmp_freq,'-','LineWidth',plotVars.plotLineThicknessHisto);

%-- scale the yaxis to give the bars some space
ylim([0 1.05])
if plotOpts.addGraphTitle==1
    title('Dist^N of speed for outies')
end
xlabel 'Total speed (um/s)'
ylabel 'Normalised Frequency'
ytickformat('%.1f')

%-- Create the legend string
tmp_legendStr = {'Inside','Outside'};
legend(tmp_legendStr,'Location','NE');

%-- Check whether anything needs to be saved. Will also leave the plot open if not.
if (plotOpts.saveFIG+plotOpts.savePNG)>0;
    saveplot(tmp_f1, '16_OUTIES_INTRA_SPEED_RATIO');
    close(tmp_f1);
end

clearvars tmp_*

%%
%---------------------------------------------------------------
% -- (17) INTRA-TRACK in/out ratios for mean track radial speed
%---------------------------------------------------------------

tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 1200 500]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting
subplot(1,2,2)

%-- first exclude anything that doesn't cross the boundary (IE outies with no innies or innies with no outies)
tmp_1=data_TrackDetails_Out(~isnan(data_TrackDetails_Out(:,9).*data_TrackDetails_Out(:,10)),9:10);

%-- calculate the ratio in/out: IE >1 means faster inside
tmp_1(:,3)=tmp_1(:,1)./tmp_1(:,2);

%-- calculate histogram and plot
tmp_binNum=floor(sqrt(size(tmp_1,1)));

[tmp_freq,tmp_bins]=hist(tmp_1(tmp_1(:,3)<=50 & tmp_1(:,3)>-50,3),tmp_binNum); %-- trim ratios from 0-20
%[tmp_freq,tmp_bins]=hist(tmp_1(:,3),tmp_binNum); %-- all the data

bar(tmp_bins,tmp_freq,'edgecolor','none')
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
%-- scale the yaxis to give the bars some space
ylim([0 floor(max(tmp_freq)*1.05)])

%-- add a line showing '1' and embelish the plot
hold on
plot([0 0],[0 max(tmp_freq)*1.05],'r-','LineWidth',plotVars.plotLineThickness)
if plotOpts.addGraphTitle==1
    title(['Dist^N of radial speed ratios'])
end
xlabel 'Paired Ratio of outie in/out speed'
ylabel 'Frequency'

%---------------------------------------------------------------
%-- add the normalised distribution of radial speeds for inside and outside of outies (not paired!)

%-- get a list of tracks that have both innies and outies
tmp_TrackList=data_TrackDetails_Out(data_TrackDetails_Out(:,3).*data_TrackDetails_Out(:,4)>0,1);
tmp_spotsHaveBoth=data_sPos(ismember(data_sPos(:,5),tmp_TrackList),:);
%-- now split into inside and outside spots
tmp_spotsHaveBoth_In=tmp_spotsHaveBoth(tmp_spotsHaveBoth(:,8)<boundary,:);
tmp_spotsHaveBoth_Out=tmp_spotsHaveBoth(tmp_spotsHaveBoth(:,8)>boundary,:);

%-- use the smaller of the two arrays to pick the binNum
tmp_binNum=floor( sqrt( min( [ size(tmp_spotsHaveBoth_In,1) , size(tmp_spotsHaveBoth_Out,1)] ) ) );

subplot(1,2,1)
[tmp_freq,tmp_bins]=hist(tmp_spotsHaveBoth_In(:,24),tmp_binNum);
tmp_freq=tmp_freq./max(tmp_freq);
plot(tmp_bins,tmp_freq,'-','LineWidth',plotVars.plotLineThicknessHisto);
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
%-- repeat for the outside of outies
hold on
[tmp_freq,tmp_bins]=hist(tmp_spotsHaveBoth_Out(:,24),tmp_binNum);
tmp_freq=tmp_freq./max(tmp_freq);
plot(tmp_bins,tmp_freq,'-','LineWidth',plotVars.plotLineThicknessHisto);

%-- scale the yaxis to give the bars some space
ylim([0 1.05])
if plotOpts.addGraphTitle==1
    title(['Dist^N of radial speed for outies'])
end
xlabel 'Radial speed (um/s)'
ylabel 'Normalised Frequency'
ytickformat('%.1f')

%-- Create the legend string
tmp_legendStr = {'Inside','Outside'};
legend(tmp_legendStr,'Location','NE');

%-- Check whether anything needs to be saved. Will also leave the plot open if not.
if (plotOpts.saveFIG+plotOpts.savePNG)>0;
    saveplot(tmp_f1, '17_OUTIES_INTRA_RAD_SPEED_RATIO');
    close(tmp_f1);
end

clearvars tmp_*

%%
%---------------------------------------------------------------
% -- (18) INTRA-TRACK in/out ratios for mean track windowed straightness
%---------------------------------------------------------------

tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 1200 500]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting
subplot(1,2,2)

%-- first exclude anything that doesn't cross the boundary (IE outies with no innies or innies with no outies) these are zeroes
tmp_1=data_TrackDetails_Out((data_TrackDetails_Out(:,17).*data_TrackDetails_Out(:,18)~=0),17:18);
%-- calculate the ratio in/out: IE >1 means straighter inside
tmp_1(:,3)=log(tmp_1(:,1)./tmp_1(:,2));

%-- calculate histogram and plot
tmp_binNum=floor(sqrt(size(tmp_1,1)));

%[tmp_freq,tmp_bins]=hist(tmp_1(tmp_1(:,3)<=20 & tmp_1(:,3)>0,3),tmp_binNum); %-- trim ratios from 0-20
[tmp_freq,tmp_bins]=hist(tmp_1(:,3),tmp_binNum); %-- all the data

bar(tmp_bins,tmp_freq,'edgecolor','none')
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
%-- scale the yaxis to give the bars some space
ylim([0 floor(max(tmp_freq)*1.1)])

%-- add a line showing '1' and embelish the plot
hold on
plot([0 0],[0 max(tmp_freq)*1.1],'r-','LineWidth',plotVars.plotLineThickness)
if plotOpts.addGraphTitle==1
    title(['Dist^N of straightness ratios'])
end
xlabel 'Paired Ratio of inside/outside straightness'
ylabel 'Frequency'

%---------------------------------------------------------------
%-- add the normalised Dist^N of speeds for inside and outside of outies (not paired!)

%-- get a list of tracks that have both innies and outies
tmp_TrackList=data_TrackDetails_Out(data_TrackDetails_Out(:,3).*data_TrackDetails_Out(:,4)>0,1);
tmp_spotsHaveBoth=data_sPos(ismember(data_sPos(:,5),tmp_TrackList),:);
%-- now split into inside and outside spots
tmp_spotsHaveBoth_In=tmp_spotsHaveBoth(tmp_spotsHaveBoth(:,8)<boundary,:);
tmp_spotsHaveBoth_Out=tmp_spotsHaveBoth(tmp_spotsHaveBoth(:,8)>boundary,:);

%-- remove zeros from window analysis
tmp_spotsHaveBoth_In=tmp_spotsHaveBoth_In(tmp_spotsHaveBoth_In(:,12)>0,:);
tmp_spotsHaveBoth_Out=tmp_spotsHaveBoth_Out(tmp_spotsHaveBoth_Out(:,12)>0,:);

%-- use the smaller of the two arrays to pick the binNum
tmp_binNum=floor( sqrt( min( [ size(tmp_spotsHaveBoth_In,1) , size(tmp_spotsHaveBoth_Out,1)] ) ) );

subplot(1,2,1)
[tmp_freq,tmp_bins]=hist(tmp_spotsHaveBoth_In(:,12),tmp_binNum);
tmp_freq=tmp_freq./max(tmp_freq);
plot(tmp_bins,tmp_freq,'-','LineWidth',plotVars.plotLineThicknessHisto);
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
%-- repeat for the outside of outies
hold on
[tmp_freq,tmp_bins]=hist(tmp_spotsHaveBoth_Out(:,12),tmp_binNum);
tmp_freq=tmp_freq./max(tmp_freq);
plot(tmp_bins,tmp_freq,'-','LineWidth',plotVars.plotLineThicknessHisto);

%-- scale the yaxis to give the bars some space
ylim([0 1.05])
if plotOpts.addGraphTitle==1
    title(['Dist^N of straightness for outies'])
end
xlabel 'Windowed straightness'
xtickformat('%.1f')
ylabel 'Normalised Frequency'
ytickformat('%.1f')

%-- Create the legend string
tmp_legendStr = {'Inside','Outside'};
legend(tmp_legendStr,'Location','SE');

%-- Check whether anything needs to be saved. Will also leave the plot open if not.
if (plotOpts.saveFIG+plotOpts.savePNG)>0;
    saveplot(tmp_f1, '18_OUTIES_INTRA_STRAIGHTNESS_RATIO');
    close(tmp_f1);
end

clearvars tmp_*

%%
%---------------------------------------------------------------
% -- (19) Plot the non-paired radial velocity for inside portions of outies and innies
%---------------------------------------------------------------
 
%-- Plot the graph
tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

%-- get a list of tracks that have both innies and outies
tmp_TrackList=data_TrackDetails_Out(data_TrackDetails_Out(:,3).*data_TrackDetails_Out(:,4)>0,1);
tmp_spotsHaveBoth=data_sPos(ismember(data_sPos(:,5),tmp_TrackList),:);
%-- now select just inside parts of outies
tmp_spotsHaveBoth_In=tmp_spotsHaveBoth(tmp_spotsHaveBoth(:,8)<boundary,:);

%-- now make an equivalent dataset for the innies
tmp_TrackList=unique(data_TrackDetails_In(:,1));
tmp_spotsHaveInnies=data_sPos(~ismember(data_sPos(:,5),tmp_TrackList),:);

%-- remove zeros from window analysis
tmp_spotsHaveBoth_In=tmp_spotsHaveBoth_In(tmp_spotsHaveBoth_In(:,10)>0,:);
tmp_spotsHaveInnies=tmp_spotsHaveInnies(tmp_spotsHaveInnies(:,10)>0,:);

%-- use the smaller of the two arrays to pick the binNum
tmp_binNum=floor( sqrt( min( [ size(tmp_spotsHaveBoth_In,1) , size(tmp_spotsHaveInnies,1)] ) ) );

[tmp_freq,tmp_bins]=hist(tmp_spotsHaveBoth_In(:,10),tmp_binNum);
tmp_freq=tmp_freq./max(tmp_freq);
plot(tmp_bins,tmp_freq,'-','LineWidth',plotVars.plotLineThicknessHisto);
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
%-- repeat for the innies
hold on
[tmp_freq,tmp_bins]=hist(tmp_spotsHaveInnies(:,10),tmp_binNum);
tmp_freq=tmp_freq./max(tmp_freq);
plot(tmp_bins,tmp_freq,'-','LineWidth',plotVars.plotLineThicknessHisto);

%-- scale the yaxis to give the bars some space
ylim([0 1.05])
if plotOpts.addGraphTitle==1
    title(['Radial Speed of Innies versus Inside of Outies'])
end
xlabel 'Radial Speed (um/s)'
ylabel 'Normalised Frequency'
ytickformat('%.1f')

%-- Create the legend string
tmp_legendStr = {'Outies (Inside)','Innies'};
legend(tmp_legendStr,'Location','SE');

%-- Check whether anything needs to be saved. Will also leave the plot open if not.
if (plotOpts.saveFIG+plotOpts.savePNG)>0;
    saveplot(tmp_f1, '19_COMPARE_INSIDES');
    close(tmp_f1);
end

clearvars tmp*



%%
%---------------------------------------------------------------
% -- (20) Plot the MSD for all tracks
%---------------------------------------------------------------

%-- Plot the graph
tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

%-- create the X and Y arrays
tmp_msd_x=[0:size(data_MSD_all,2)-1]*tCalib;
tmp_msd_y=nanmean(data_MSD_all,1);
%-- count the number of values going into each tau
tmp_counts=sum(~isnan(data_MSD_all),1);

tmp_h=plot(tmp_msd_x,tmp_msd_y,'k-','LineWidth',plotVars.plotLineThickness);
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);
xlabel 'Tau (s)'
ylabel 'MSD (\mum^2)'
xtickformat('%.1f')

%-- add SEM as a shaded area
tmp_SEM=nanstd(data_MSD_all,1)./(sqrt(tmp_counts));
hold on
fill([tmp_msd_x,fliplr(tmp_msd_x)],[tmp_msd_y+tmp_SEM,fliplr(tmp_msd_y-tmp_SEM)],'k','EdgeColor',[0.3,0.3,0.3],'FaceColor',[0.8,0.8,0.8]);

%-- move plot to the top
uistack(tmp_h,'top');

if plotOpts.addGraphTitle==1
    title(['MSD plot for all tracks'])
end

%-- Check whether anything needs to be saved. Will also leave the plot open if not.
if (plotOpts.saveFIG+plotOpts.savePNG)>0;
    saveplot(tmp_f1, '20_MSD_ALLTRACKS');
    close(tmp_f1);
end

%-- calculate the Diffusion Coefficient
[tmp_a,tmp_b,tmp_c]=fit(tmp_msd_x(1:10)',tmp_msd_y(1:10)','poly1');
disp(['Estimated Diffusion Coefficient: ',num2str(tmp_a.p1/6),' um^2/s'])
clearvars tmp*

%%
%---------------------------------------------------------------
% -- (21) Plot the MSD for in vs out tracks
%---------------------------------------------------------------

%-- Plot the graph
tmp_f1=figure('Color',[1 1 1]);
set(gcf,'position',[100 100 800 600]) %-- Set the figure position and size of the figure
movegui(gcf,'center') %-- Do this to make sure that the figure isn't cut off (or including other windows) when exporting

%-- create the X and Y arrays
tmp_msd_x_in=[1:size(data_MSD_in,2)]*tCalib;
tmp_msd_x_out=[1:size(data_MSD_out,2)]*tCalib;
tmp_msd_y_in=nanmean(data_MSD_in,1);
tmp_msd_y_out=nanmean(data_MSD_out,1);
%-- count the number of values going into each tau
tmp_counts_in=sum(~isnan(data_MSD_in),1);
tmp_counts_out=sum(~isnan(data_MSD_out),1);

%-- plot innies
tmp_h1=plot(tmp_msd_x_in,tmp_msd_y_in,'-','LineWidth',plotVars.plotLineThickness);
hold on
tmp_h2=plot(tmp_msd_x_out,tmp_msd_y_out,'-','LineWidth',plotVars.plotLineThickness);
legend({['Innies (',num2str(size(data_MSD_in,1)),' segments)'],['Outies (',num2str(size(data_MSD_out,1)),' segments)']},'Location','NorthWest')
xlabel 'Tau (s)'
ylabel 'MSD (\mum^2)'
xtickformat('%.1f')
set(gca,'fontsize',plotVars.axisFontSize, 'LineWidth',plotVars.axisLineWidth);

%-- add SEM as a shaded area
tmp_SEM_in=nanstd(data_MSD_in,1)./(sqrt(tmp_counts_in));
tmp_SEM_out=nanstd(data_MSD_out,1)./(sqrt(tmp_counts_out)); 
%-- plot SEM, turning off handle visibility so it doesn't appear in the legend
fill([tmp_msd_x_in,fliplr(tmp_msd_x_in)],[tmp_msd_y_in+tmp_SEM_in,fliplr(tmp_msd_y_in-tmp_SEM_in)],'k','EdgeColor','none','FaceColor',[0,0,1],'FaceAlpha',0.2,'HandleVisibility','off');
fill([tmp_msd_x_out,fliplr(tmp_msd_x_out)],[tmp_msd_y_out+tmp_SEM_out,fliplr(tmp_msd_y_out-tmp_SEM_out)],'k','EdgeColor','none','FaceColor',[1,0,0],'FaceAlpha',0.2,'HandleVisibility','off');

%-- move plots above shaded areas
uistack(tmp_h2,'top');
uistack(tmp_h1,'top');

if plotOpts.addGraphTitle==1
    title(['MSD plot for in vs out tracks'])
end

%-- Check whether anything needs to be saved. Will also leave the plot open if not.
if (plotOpts.saveFIG+plotOpts.savePNG)>0;
    saveplot(tmp_f1, '21_MSD_IN_OUT');
    close(tmp_f1);
end

%-- calculate the Diffusion Coefficient
[tmp_a,tmp_b,tmp_c]=fit(tmp_msd_x_in(1:10)',tmp_msd_y_in(1:10)','poly1');
disp(['Estimated Diffusion Coefficient (In): ',num2str(tmp_a.p1/6),' um^2/s'])
[tmp_a,tmp_b,tmp_c]=fit(tmp_msd_x_out(1:10)',tmp_msd_y_out(1:10)','poly1');
disp(['Estimated Diffusion Coefficient (Out): ',num2str(tmp_a.p1/6),' um^2/s'])


clearvars tmp*


%%
%---------------------------------------------------------------
% -- (X) All done
%---------------------------------------------------------------
%-- recover warning status
warning(w);clearvars w;
disp(['[' datestr(now,31),'] ---------/ ',mfilename,' ----------']);

%%
%---------------------------------------------------------------
% -- (F) SAVE PLOT FUNCTIONS
%---------------------------------------------------------------

function saveplot(figHandle, fNameSuffix)

%-- check for output folder and create if !exist
    if exist([evalin('base','inFileDir_processed'),['output_',strrep(evalin('base','inFile_processed'),'.mat','')]],'dir') == 0
        mkdir([evalin('base','inFileDir_processed'),['output_',strrep(evalin('base','inFile_processed'),'.mat','')]]);
    end
    
%-- catch lack of parameters here

if evalin('base','plotOpts.saveFIG')==1
	savefig(figHandle,[[evalin('base','inFileDir_processed'),['output_',strrep(evalin('base','inFile_processed'),'.mat','')]],filesep,strrep(evalin('base','inFile_processed'),'_processed.mat',['_',fNameSuffix,'.fig'])]);
end

if evalin('base','plotOpts.savePNG')==1
	saveas(figHandle,[[evalin('base','inFileDir_processed'),['output_',strrep(evalin('base','inFile_processed'),'.mat','')]],filesep,strrep(evalin('base','inFile_processed'),'_processed.mat',['_',fNameSuffix,'.png'])]);
end

end