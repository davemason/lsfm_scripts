%% POST-PROCESSING SCRIPT FOR TRACKED LIGHTSHEET DATA
%
%   Script to calculate various statistics from Imaris ('Position' output) or Trackmate ('Spots in tracks statistics') tracking data output.
%
%
%                                           Written By Dave Mason [dnmason@liv.ac.uk]
%   										Centre for Cell Imaging [cci.liv.ac.uk]
%   										University of Liverpool
%
%   OUTPUT:
%   =======
%   The array "data_sPos" will be created upon import (first 5 columns) will have one row per spot and the following columns:
%
%   COLUMN      CONTENT
%   1           x-position (um)
%   2           y-position (um)
%   3           z-position (um)
%   4           frame number
%   5           Track ID
%   6           Spot Azimuth
%   7           Spot Elevation
%   8           Spot Radial distance (um)
%   9           Step displacements (forward) (um)
%   10          Step Speed (forward) (um/s)
%   11          Windowed Speed (forward) (um/s)
%   12          Windowed Straightness (forward) (um/s)
%   13          x-displacement (um)
%   14          y-displacement (um)
%   15          z-displacement (um)
%   16          x-component of speed (um/s)
%   17          y-component of speed (um/s)
%   18          z-component of speed (um/s)
%   19          azimuth angle (from origin) in radians
%   20          elevation angle (from origin) in radians
%   21          radial dist (um)
%   22          logitudinal speed (azimuthal) (um/s)
%   23          lateral speed (elevatorial) (um/s)
%   24          radial speed (um/s)

%%
%---------------------------------------------------------------
% -- (1) Input and global params
%---------------------------------------------------------------
clearvars
close all
clc
disp(['[' datestr(now,31),'] ---------- ',mfilename,' ----------']);
disp(['[' datestr(now,31),'] Initialising Script and gathering params']);

%-- Store the warning state, then turn off all warnings
w=warning;
warning('off','all');

%-- Ask the user for an input file in csv format
[inFile,inFileDir]=uigetfile('../*.csv');

%-- check to make sure the dialog has not been cancelled
if inFile==0
    disp(['[' datestr(now,31),'] ERROR! No data file provided']);
    return
end

%-- Ask for the format 
inFormat=questdlg('In what format is the input CSV?','Format', 'Imaris', 'Trackmate', 'Imaris');

%-- Get user input for centroid position
disp(['[' datestr(now,31),'] Requesting parameters for file: ',inFile]);
tmp_inCentroid=inputdlg({'Centroid X position','Centroid Y position','Centroid Z position','Time Interval (s)'},'Enter Parameters',1);

%-- check to make sure the dialog has not been cancelled  
if isempty(tmp_inCentroid)
   disp(['[' datestr(now,31),'] [ ERROR ] Centroid Coordinates Required for processing']);
   return
end
    
centrePosXYZ=[str2num(tmp_inCentroid{1}),str2num(tmp_inCentroid{2}),str2num(tmp_inCentroid{3})];
tCalib=str2num(tmp_inCentroid{4});

%-- Sanity check
if isempty(centrePosXYZ) || isempty(tCalib)
    disp(['[' datestr(now,31),'] [ ERROR ] Centroid Coordinates Required for processing']);
    return
end

clearvars tmp_inCentroid;
disp(['[' datestr(now,31),']     ...done'])

%%
%---------------------------------------------------------------
% -- (2) Load in the data
%---------------------------------------------------------------
% -- Import the Data
disp(['[' datestr(now,31),'] Starting Import'])
data_sPos = xlsread([inFileDir,inFile]);
disp(['[' datestr(now,31),']     ...done'])

disp(['[' datestr(now,31),'] Formatting input from ',inFormat])

%-- preprocess the data so that we have : PosX,PosY,PosZ,Frame,tID
switch inFormat
    case 'Imaris'
        %-- correct as of 2018-08-01 Imaris v 9.2 'Position' output
        data_sPos=data_sPos(:,~isnan(data_sPos(1,:)));
        data_sPos=data_sPos(:,1:5);
    case 'Trackmate'
        %-- correct as of 2018-08-01 Trackmate v 3.8 'Spots in tracks statistics' Output
        data_sPos=[data_sPos(:,4:6),data_sPos(:,8),data_sPos(:,2)];
        %-- add 10^9 to Track ID to match Imaris formatting
        data_sPos(:,5)=data_sPos(:,5)+10^9;
end
%-- sort the data by trackID then timepoint
data_sPos=sortrows(data_sPos,[5,4]);
disp(['[' datestr(now,31),']     ...done'])

%%
%---------------------------------------------------------------
% -- (3) Calculate the Radial Position
%---------------------------------------------------------------
disp(['[' datestr(now,31),'] Converting cartesian to spherical coordinates'])

%-- Convert the cartesian coordinates of sPos into spherical system after subtracting centre point offset
[data_sPos(:,6),data_sPos(:,7),data_sPos(:,8)]=cart2sph(data_sPos(:,1)-centrePosXYZ(1),data_sPos(:,2)-centrePosXYZ(2),data_sPos(:,3)-centrePosXYZ(3));

disp(['[' datestr(now,31),']     ...done'])

%%
%---------------------------------------------------------------
% -- (4) Calculate the Spheroid Density
%---------------------------------------------------------------
disp(['[' datestr(now,31),'] Calculating the spheroid density'])

%-- how many bins do you want for the density calculations
tmp_numBins=20;
%-- over what distance should the density plot run
tmp_radius=400;
%-- what angle of elevation should we use for the conic section (in degrees)
tmp_angle=30;
%-- from which frame?
tmp_frame=max(data_sPos(:,4)); %-- last frame in the movie
%tmp_frame=1;

%-- pull the specified frame data to work with
tmp_f1spots=data_sPos(data_sPos(:,4)==tmp_frame,:);
%-- Filter the data based on elevation
tmp_f1spots_filter=tmp_f1spots(tmp_f1spots(:,7)>deg2rad(tmp_angle),:);
%-- Create the linear bins
tmp_linspace=linspace(0,tmp_radius,tmp_numBins);

%-- for each linear bin, calculate the number of spots in that bin
for tmp_densityBin=1:tmp_numBins-1
    %-- count cells
    tmp_density(tmp_densityBin,1)=sum(tmp_f1spots_filter(:,8)>tmp_linspace(tmp_densityBin) & tmp_f1spots_filter(:,8)<tmp_linspace(tmp_densityBin+1));
    
    %-- calculate the volume of the conic section
    %-- V= (2/3)*pi*(radius^2)*(height of spherical cap) FROM: http://mathworld.wolfram.com/SphericalCone.html
    %-- AND https://en.wikipedia.org/wiki/Spherical_cap
    %-- Store the outer and inner radius for this bin
    tmp_outRadius=tmp_linspace(tmp_densityBin+1);
    tmp_inRadius=tmp_linspace(tmp_densityBin);
    %-- calculate the volume of the bigger conic section, then subtract the smaller to just get the frustrum (?) of the conic section
    tmp_density(tmp_densityBin,2)=((2/3)*pi*(tmp_outRadius^2)*(tmp_outRadius-(tmp_outRadius*cos(deg2rad(tmp_angle))))) - ((2/3)*pi*(tmp_inRadius^2)*(tmp_inRadius-(tmp_inRadius*cos(deg2rad(tmp_angle)))));
    
    %-- calculate the cells / volume
    tmp_density(tmp_densityBin,3)=tmp_density(tmp_densityBin,1)/tmp_density(tmp_densityBin,2);
end

%-- TESTING ONLY
if (0   )
    plot(tmp_linspace(1:end-1),tmp_density(:,3),'o-')
    xlabel 'Radial Distance (\mum)'
    ylabel 'Density (cells/volume)'
    title(['Density (>',num2str(tmp_angle),char(186),' elevation)']);
    hold on
end

clearvars tmp_*
disp(['[' datestr(now,31),']     ...done'])

%%
%---------------------------------------------------------------
% -- (5) Calculate the cartesian step displacements
%---------------------------------------------------------------
disp(['[' datestr(now,31),'] Calculating Step Displacements'])

%-- Calculate the diffs
tmp1=diff(data_sPos(:,1:3));
%-- Square them
tmp2=tmp1.^2;
%-- Sum across XYZ
tmp3=sum(tmp2,2);
%-- Sqrt
tmp4=sqrt(tmp3);

%-- remove the inter-track diffs
%-- tmp5 will have a 1 at the last frame of a track
tmp5=logical(diff(data_sPos(:,5)));
tmp4((tmp5==1))=nan;
data_sPos(:,9)=[tmp4;nan];

clearvars tmp*;
disp(['[' datestr(now,31),']     ...done'])

%%
%---------------------------------------------------------------
% -- (6) Calculate the MSD for all tracks
%---------------------------------------------------------------
disp(['[' datestr(now,31),'] Calculating Track MSD'])

%-- create an array in which to store the MSD values
data_MSD_all=[];

%-- get a list of unique tracks through which to iterate
tmp_trackList=sortrows(unique(data_sPos(:,5)));

%-- loop tracks
for tmp_track=1:size(tmp_trackList,1);
    %-- pull the positional data only for this track
    tmp_seg=data_sPos(data_sPos(:,5)==tmp_trackList(tmp_track),[1:3]);

%-- using 25% of tracklength for taus so make sure there is at least 1 taus to use (4 frames or greater)
if size(tmp_seg,1)>3
    %-- for this track record the row to use in the output
    tmp_row=size(data_MSD_all,1)+1;
end

for tmp_tau=1:floor(size(tmp_seg,1)*0.25)
    %-- calculate the linear diffs for time steps of tau
    tmp_MSD=diff(tmp_seg(1:tmp_tau:end,:));
    %-- square the diffs
    tmp_MSD=tmp_MSD.^2;
    %-- sum across rows
    tmp_MSD=sum(tmp_MSD,2);

    %-- calculate mean for this tau
    data_MSD_all(tmp_row,tmp_tau)=mean(tmp_MSD);

end %-- tau loop
    
end %-- loop tracks

%-- so we can calculate the means later, change all padding zeros to NaN
data_MSD_all(data_MSD_all==0)=NaN;

clearvars tmp*;
disp(['[' datestr(now,31),']     ...done'])

%%
%---------------------------------------------------------------
% -- (7) Calculate the Step speed (will be very noisy)
%---------------------------------------------------------------
disp(['[' datestr(now,31),'] Calculating Step Speed'])

data_sPos(:,10)=data_sPos(:,9)./tCalib;

disp(['[' datestr(now,31),']     ...done'])

%%
%---------------------------------------------------------------
% -- (8) Calculate the instantaneous windowed straightness and speed of spots
%---------------------------------------------------------------

disp(['[' datestr(now,31),'] Processing Windowed Spot Straightness and Speed'])

%-- sort the data by trackID then timepoint (should be done already in part 2)
data_sPos=sortrows(data_sPos,[5,4]);

%-- What window should be used to calculate the spots? 
%-- use the windNum forward, ignoring the last (windNum-1) values
windNum=5;

tmp_wb=waitbar(0,'Initialising...');

for i=1:length(data_sPos(:,1))-windNum
    
    %-- Check to make sure that there are windNum features and that the last displacement isn't an NaN (IE the last displacement of the track)
    if (data_sPos(i,5)~=data_sPos(i+windNum-1,5)) || isnan(data_sPos(i+windNum-1,9))
            continue
    end
    
    %-- we have at least windNum points ahead so calculate the straightness and speed    
    %-- Calculate the end-to-end displacement
    tmp1=sqrt( (data_sPos(i,1)-data_sPos(i+windNum,1))^2 + (data_sPos(i,2)-data_sPos(i+windNum,2))^2 + (data_sPos(i,3)-data_sPos(i+windNum,3))^2 );
    
    %-- record the windowed speed in um/s
    data_sPos(i,11)=tmp1/(tCalib*windNum);
        
    %-- Calculate the path length (ahead)
    tmp2=sum(data_sPos(i:i+windNum-1,9));
    
    %-- Record the straightness
    data_sPos(i,12)=tmp1/tmp2;
    
    %-- update the waitbar every 10000 spots
    if mod(i, 10000) == 0
    waitbar(i/length(data_sPos(:,1)),tmp_wb,sprintf('%0.1f%% done analysing windowed spot parameters',(i/length(data_sPos(:,1)))*100));
    end
end
close(tmp_wb);

%-- replace zeros with NaN so we don't mess up calculations later
data_sPos(data_sPos(:,11)==0,11:12)=nan;

clearvars tmp* i windNum;
disp(['[' datestr(now,31),']     ...done'])

%%
%---------------------------------------------------------------
% -- (9) Calculate the Cumlulative Distribution by distance over time
%---------------------------------------------------------------

disp(['[' datestr(now,31),'] Processing Cumulative Distribution by Distance']);

binNum=50;
timePointsArray=[1 160 320 480]; %-- Timepoints are hardcoded to number of frames for comparison of data between experiments

%-- Set up array with bins based on the maximum (radial) distance of spots
data_CDF(:,1)=[0 : (max(data_sPos(:,8))*1.1)/binNum : max(data_sPos(:,8))*1.1];

%-- Do numPoints worth of timepoints
for j=timePointsArray
%-- Loop through position file and collate corresponding speed value
for i=1:(binNum+1)
    
    %-- For each timepoint, calculate the cululative number of spots within that distance range
    data_CDF(i,find(timePointsArray==j)+1)=length(data_sPos(data_sPos(:,8)<data_CDF(i,1) & data_sPos(:,4)==j) );
    
end %-- i loop
end %-- j loop

%-- Check the graphs with:
%figure
%plot(data_CDF(:,1),data_CDF(:,[2:5]),'-');

%-- Normalise the data in Y for comparison
data_CDFnorm(:,1)=data_CDF(:,1);

for j=2:length(data_CDF(1,:))

    data_CDFnorm(:,j)=(data_CDF(:,j)-min(data_CDF(:,j)))/(max(data_CDF(:,j))-min(data_CDF(:,j)));

end

%-- Check the graphs with:
%figure
%plot(data_CDFnorm(:,1),data_CDFnorm(:,[2:5]),'-');

clearvars i j binNum timePointsArray;
disp(['[' datestr(now,31),']     ...done'])

%%
%---------------------------------------------------------------
% -- (10) Calculate boundary distance based on frame one locations
%---------------------------------------------------------------
disp(['[' datestr(now,31),'] Calculating Boundary']);

%-- number of spots to use for consideration of the boundary
numSpotsForBoundary=10; %-- use this many points to calculate the boundary
numSpotsForBoundaryIgnore=10; %-- ignore the first X spots (important if you have outliers). If you're sure there are none in the first frame, this can be zero.

%-- Take the furthest out spots in the first frame (minus potential outliers) and use the mean as the in/out boundary
tmp_1=sortrows(data_sPos(data_sPos(:,4) == 1,:),8,'descend');
boundary=mean(tmp_1(numSpotsForBoundaryIgnore+1:numSpotsForBoundaryIgnore+numSpotsForBoundary,8));

%-- Refine the Boundary here (add 5% to distance)
boundary=boundary*1.05;

clearvars tmp_1 numSpotsForBoundary*;
disp(['[' datestr(now,31),']     ...done'])

%%
%---------------------------------------------------------------
% -- (11) Calculate the cartesian velocity components
%   (fairly meaningless in a spheroid and should have a mean distribution of about zero)
%---------------------------------------------------------------
disp(['[' datestr(now,31),'] Calculating cartesian speed components']);

%-- Calculate the diffs
tmp_1=diff(data_sPos(:,1:3));

%-- if you want the total speed, take the values, square then sqrt them to remove negatives

%-- remove the inter-track diffs
%-- tmp_logical will have a 1 at the last frame of a track (IE trackID changes)
tmp_logical=logical(diff(data_sPos(:,5)));

%-- convert the last diff into a NaN in all three columns
tmp_1(tmp_logical==1,1:3)=nan;
%-- add one row of NaN to the end of each column to make the matricies consistent length
tmp_1=[tmp_1;[nan,nan,nan]];

%-- concat the the end of the data structure.
%-- ROWS 13, 14, 15 are the X,Y,Z displacements
data_sPos(:,13:15)=tmp_1;

%-- speed along each axis in um/s %-- ASSUMES CONSISTANT T THROUGHOUT
data_sPos(:,16:18)=data_sPos(:,13:15)./tCalib;

clearvars tmp_*;
disp(['[' datestr(now,31),']     ...done'])

%%
%---------------------------------------------------------------
% -- (12) Calculate the radial components of the step speed
%---------------------------------------------------------------
%-- This adds columns 19-24 to data_sPos:
% az angle       |
% el angle       | Already calculated as columns 6-8, can remove later
% radial dist    |
% az step speed
% el step speed
% radial step speed

disp(['[' datestr(now,31),'] Calculating radial speed components']);

%-- Convert the cartesian coordinates of sPos into spherical system after subtracting centre point offset (cart2sph works from origin)
[data_sPos(:,19),data_sPos(:,20),data_sPos(:,21)]=cart2sph(data_sPos(:,1)-centrePosXYZ(1),data_sPos(:,2)-centrePosXYZ(2),data_sPos(:,3)-centrePosXYZ(3));

%-- use the starting point of the step vector to define the radial angle using the az and el
%-- NOTE: this doesn't work if the start point of any vector has an azimuth or elevation of zero (IE it is at the centre point). Likely never a problem in reality.
tmp_wb = waitbar(0,'Initialising...');
tmp_loopEnd=size(data_sPos(:,1),1);
for i=1:tmp_loopEnd
    if ~isnan(data_sPos(i,16))
        data_sPos(i,22:24) = cart2sphvec(data_sPos(i,16:18)',rad2deg(data_sPos(i,19)),rad2deg(data_sPos(i,20)))';
    else
        data_sPos(i,22:24) = [nan nan nan];
    end
    if mod(i,10000)==0 %-- any updates more than this slows the code
        waitbar(i/length(data_sPos(:,1)),tmp_wb,sprintf('%0.1f%% done calculating Spherical Velocity Vectors',(i/length(data_sPos(:,1)))*100));
    end
end

close(tmp_wb);
clearvars tmp* i
disp(['[' datestr(now,31),']     ...done'])


%%
%------------------------------------------------------
% -- (13) CATEGORISE TRACKS INTO INSIDE ONLY / OUTSIDE AT ALL
%------------------------------------------------------
disp(['[' datestr(now,31),'] Categorise tracks based on distance from centre'])

%-- First get a list of all unique trackIDs that have _any_ point outside boundary + a bit
tmp_boundaryPlus=20;
%-- (this excludes issues with cells getting to edge, popping out then back - not really leaving especially if the spheroid has shrunk).
%-- may also want to consider at least X points eg. https://uk.mathworks.com/matlabcentral/answers/171141
data_TrackDetails_Out=sortrows(unique(data_sPos(data_sPos(:,8)>boundary+tmp_boundaryPlus,5)));

%-- Make a logical array of 'outside' tracks
%-- note that these can have inside components too
tmp_keepArray=ismember(data_sPos(:,5),data_TrackDetails_Out);

%-- make two arrays of tracks with all columns of data_sPos  (this is removed at the end of 12)
data_sPos_Tracks_out=data_sPos(tmp_keepArray,:);
data_sPos_Tracks_in=data_sPos(~tmp_keepArray,:);

%-- make a corresponding output array of inside only:
data_TrackDetails_In=sortrows(unique(data_sPos_Tracks_in(:,5)));

clearvars tmp*
disp(['[' datestr(now,31),']     ...done'])

%%
%------------------------------------------------------
% -- (14) CALCULATE PARAMETERS FOR IN/OUT COMPONENTS OF TRACKS
%------------------------------------------------------
disp(['[' datestr(now,31),'] Calculating parameters for track in/out components'])

%-- We now have 
%data_TrackListOut_Num=size(unique(data_sPos_Tracks_out(:,5)),1);
%data_TrackListIn_Num=size(unique(data_sPos_Tracks_in(:,5)),1);

%-- for each track report [ data_TrackDetails_{In|Out} ]:
% 1 TrackID             10^9 padded from Imaris, integers from Trackmate
% 2 totalPoints         Number of features in track
% 3 inPoints            Features inside spheroid
% 4 outPoints           Features outside spheroid
% 5 azVelIn             Mean 1-step lateral velocity of inside parts
% 6 azVelOut            Mean 1-step lateral velocity of outside parts
% 7 elVelIn             Mean 1-step longitudinal velocity of inside parts
% 8 elVelOut            Mean 1-step longitudinal velocity of outside parts
% 9 radVelIn            Mean 1-step radial velocity of inside parts
% 10 radVelOut          Mean 1-step radial velocity of outside parts
% 11 segments inside    Number of contiguous inside segments
% 12 segments outside   Number of contiguous outside segments
% 13 straightIn         Mean straightness of inside segments
% 14 straightOut        Mean straightness of outside segments
% 15 speedIn            Mean speed of inside segments
% 16 speedOut           Mean speed of outside segments
% 17 winStraightIn      Mean windowed straightness Inside segs
% 18 winStraightOut     Mean windowed straightness Outside segs

%-- create empty arrays for MSD analysis
%if ~exist('data_MSD_out','var');data_MSD_out=[];end
data_MSD_in=[];data_MSD_out=[];

%-- Start In / Out datasets loop here
for j=1:2
    if j==1
        tmp_InOut=data_TrackDetails_Out;
        tmp_wb = waitbar(0,'Characterising outies');
        disp(['[' datestr(now,31),']     ...Analysing track segments of outies'])

    else
        tmp_InOut=data_TrackDetails_In;
        tmp_wb = waitbar(0,'Characterising innies');
        disp(['[' datestr(now,31),']     ...Analysing track segments of innies'])
    end

    %-- setup variables for waitbar
    loopEnd=size(tmp_InOut,1);
    loopStep=floor(loopEnd/200)+1;
    
    %-- Loop all trajectories
for i=1:size(tmp_InOut,1)
    %-- get the track data
    if j==1
        tmp_trackData=data_sPos_Tracks_out(data_sPos_Tracks_out(:,5)==tmp_InOut(i,1),:);
    else
        tmp_trackData=data_sPos_Tracks_in(data_sPos_Tracks_in(:,5)==tmp_InOut(i,1),:);
    end
    
    %-- trackID
    output_tmp(i,1)=tmp_InOut(i,1);
    %-- total number of points
    output_tmp(i,2)=size(tmp_trackData,1);
    %-- number of points inside
    output_tmp(i,3)=size(tmp_trackData(tmp_trackData(:,8)<boundary,:),1);
    %-- number of points outside
    output_tmp(i,4)=size(tmp_trackData(tmp_trackData(:,8)>boundary,:),1);
    
    %-- for this track, average lateral velocity of the inside and outside parts
    output_tmp(i,5)=nanmean(data_sPos(data_sPos(:,5) == tmp_InOut(i) & data_sPos(:,8) < boundary,22));
    output_tmp(i,6)=nanmean(data_sPos(data_sPos(:,5) == tmp_InOut(i) & data_sPos(:,8) > boundary,22));
    %-- for this track, average longitudinal velocity of the inside and outside parts
    output_tmp(i,7)=nanmean(data_sPos(data_sPos(:,5) == tmp_InOut(i) & data_sPos(:,8) < boundary,23));
    output_tmp(i,8)=nanmean(data_sPos(data_sPos(:,5) == tmp_InOut(i) & data_sPos(:,8) > boundary,23));
    %-- for this track, average radial velocity of the inside and outside parts
    output_tmp(i,9)=nanmean(data_sPos(data_sPos(:,5) == tmp_InOut(i) & data_sPos(:,8) < boundary,24));
    output_tmp(i,10)=nanmean(data_sPos(data_sPos(:,5) == tmp_InOut(i) & data_sPos(:,8) > boundary,24));
    
    %-- calculate the (scalar) speed of inside and outside components
    %-- use nanmean for step values as the last value will always be NaN
    output_tmp(i,15)=nanmean(data_sPos(data_sPos(:,5) == tmp_InOut(i) & data_sPos(:,8) < boundary,10));
    output_tmp(i,16)=nanmean(data_sPos(data_sPos(:,5) == tmp_InOut(i) & data_sPos(:,8) > boundary,10));
    
    
    %----------------------------------------------------------------
    %-- debug only - plot the radial distance as a function of time with the boundary plotted
    if 0
        close all;figure;plot(tmp_trackData(:,4),tmp_trackData(:,8))
        hold on;plot([min(tmp_trackData(:,4)) max(tmp_trackData(:,4))],[boundary boundary],'r-')
        xlabel 'Frames'
        ylabel 'Radial Distance (um)'
        title([inFile,' : track ' num2str(tmp_InOut(i,1)-10^9)],'Interpreter','none')
    end
    %----------------------------------------------------------------
    
%-- copy this track's data and trim it segment by segment    
tmp_trk=tmp_trackData;
    
%-- clear variables that will be made when j=1
clearvars tmp_segInParts tmp_segOutParts tmp_seg crossIndex
while ~isempty(tmp_trk)

%-- does the track start inside?
if tmp_trk(1,8)<boundary
    %-- start inside
    crossIndex = find(tmp_trk(:,8)>boundary,1)-1;
    %-- check to see if this returns empty (IE track stays inside) in which
    %-- case remove the rest of the array
    if isempty(crossIndex)
        tmp_seg=tmp_trk(1:end,:);
        %-- last segment so clear the input
        tmp_trk=[];
    else
        tmp_seg=tmp_trk(1:crossIndex,:);
        %-- remove tmp_trk parts that have been processed
        tmp_trk=tmp_trk(crossIndex+1:end,:);
    end

    %-- record segment as innie (1) vs outie (0)
    tmp_segType=1;
    
else
    %-- start outside
    crossIndex = find(tmp_trk(:,8)<boundary,1)-1;
    if isempty(crossIndex)
        tmp_seg=tmp_trk(1:end,:);
        %-- last segment so clear the input
        tmp_trk=[];
    else
        tmp_seg=tmp_trk(1:crossIndex,:);
        %-- remove tmp_trk parts that have been processed
        tmp_trk=tmp_trk(crossIndex+1:end,:);
    end
    
    %-- record innie (1) vs outie (0)
    tmp_segType=0;
end

%-- process tmp_seg here if above window length

    if size(tmp_seg,1) >= 3
    
    %-- Calculate the displacement between the first and last points of the segment
    tmp_disp=( sqrt ( sum( diff([tmp_seg(1,1:3);tmp_seg(end,1:3)]).^2 ) ) );
    %-- Calculate the pathlength between the first and last points of the segment
    tmp_path=(sum(sqrt(sum(diff(tmp_seg(:,1:3)).^2,2))));
    %-- Calculate the whole segment straightness
    tmp_straightness= tmp_disp/tmp_path ;

%---------------------------------------------------
%-- Calculate the windowed straightness from tmp_seg
%---------------------------------------------------
    tmp_window=5;
    %-- calculate the number of windows in the segment
    tmp_numWindows=floor(size(tmp_seg,1)/tmp_window);
    %-- preallocate the array:
    tmp_windStraight=zeros(1,tmp_numWindows);
    
    %-- for each non-overlapping window (k) in the segment
    for k=1:tmp_numWindows
        
        %-- find the start of the k'th window for this segment
        tmp_winStart=((k-1)*tmp_window)+1;
        %-- find the end of the k'th window for this segment
        tmp_winEnd=tmp_winStart+tmp_window-1;
        %-- window displacement
        tmp_disp=( sqrt ( sum( diff([tmp_seg(tmp_winStart,1:3);tmp_seg(tmp_winEnd,1:3)]).^2 ) ) );
        %-- window pathlength
        tmp_path=(sum(sqrt(sum(diff(tmp_seg(tmp_winStart:tmp_winEnd,1:3)).^2,2))));
        %-- window straightness - check what is the optimal window size to accurately report the windowed straightness?
        tmp_windStraight(k)= tmp_disp/tmp_path;
    end
    %-- calculate the mean windowed straightness
        tmp_windStraight= mean(tmp_windStraight(:));
    
%-- record segment value, creating the array if necessary
        if tmp_segType==1 %-- segment is an innie
            if exist('tmp_segInParts','var')
                tmp_segInParts=[tmp_segInParts,tmp_straightness];
                tmp_segInPartsStraight=[tmp_segInPartsStraight,tmp_windStraight];
            else
                tmp_segInParts=tmp_straightness;
                tmp_segInPartsStraight=tmp_windStraight;
            end
        else %-- segment is an outie
            if exist('tmp_segOutParts','var')
                tmp_segOutParts=[tmp_segOutParts,tmp_straightness];
                tmp_segOutPartsStraight=[tmp_segOutPartsStraight,tmp_windStraight];
            else
                tmp_segOutParts=tmp_straightness;
                tmp_segOutPartsStraight=tmp_windStraight;
            end
        end
    end %-- close GT 3 frames check

    %-----------------------------------------------------------------------
    %-- MSD Analysis for the track segment
    %-----------------------------------------------------------------------
    %-- we will have recorded an innie (1) or an outie (0) in tmp_segType and have the data in tmp_seg
    %disp(['Using track ',num2str(tmp_trackID),' (',num2str(size(tmp_sPos,1)),' frames)'])
    %-- using 25% of tracklength for taus so make sure there is at least 1 taus to use (4 frames or greater)
    if size(tmp_seg,1)>3
    %-- for this segment record the row to use
        if tmp_segType
            tmp_row=size(data_MSD_in,1)+1;
        else
            tmp_row=size(data_MSD_out,1)+1;
        end
        
        for tmp_tau=1:floor(size(tmp_seg,1)*0.25)
            %-- calculate the linear diffs for time steps of tau
            tmp_MSD=diff(tmp_seg(1:tmp_tau:end,[1:3]));
            %-- square the diffs
            tmp_MSD=tmp_MSD.^2;
            %-- sum across rows
            tmp_MSD=sum(tmp_MSD,2);

            %-- calculate mean for this tau
            if tmp_segType
                data_MSD_in(tmp_row,tmp_tau)=mean(tmp_MSD);
            else
                data_MSD_out(tmp_row,tmp_tau)=mean(tmp_MSD);
            end
        end
    
    end %-- GT 4 length check for MSD

%-- so we can calculate the means, change all padding zeros to NaN
data_MSD_in(data_MSD_in==0)=NaN;
data_MSD_out(data_MSD_out==0)=NaN;

clearvars tmp_myval tmp_segType tmp_disp tmp_path tmp_windStraight tmp_window tmp_winStart rmp_winEnd tmp_numWindows
end %-- end segment breakdown loop

%-- record the parts
% disp(['Innie segments: ' num2str(size(tmp_segInParts,2))])
% disp(['Outie segments: ' num2str(size(tmp_segOutParts,2))])
% disp(['Innie mean: ' num2str(mean(tmp_segInParts))])
% disp(['Outie mean: ' num2str(mean(tmp_segOutParts))])

%-- as the variable is cleared each loop, an absent variable means no segments out/in side (although the array will pad with zeros)
if exist('tmp_segInParts','var')
output_tmp(i,11)=size(tmp_segInParts,2);
output_tmp(i,13)=mean(tmp_segInParts);
output_tmp(i,17)=mean(tmp_segInPartsStraight);
end
if exist('tmp_segOutParts','var')
output_tmp(i,12)=size(tmp_segOutParts,2);
output_tmp(i,14)=mean(tmp_segOutParts);
output_tmp(i,18)=mean(tmp_segOutPartsStraight);
end

%-- update waitbar
if mod(i,loopStep)==0
    waitbar(i/loopEnd,tmp_wb,sprintf('%0.1f%% done Analysing Track Segments',(i/loopEnd)*100));
end

end %-- close track loop (i)
close(tmp_wb)

%-- rename output depending upon whether we're using In or Out files
    if j==1
        data_TrackDetails_Out=output_tmp;
    else
        data_TrackDetails_In=output_tmp;
        %-- Remember that this will not have a final column (no outies!)
    end

clearvars i loopEnd loopStep output_tmp tmp_InOut crossIndex
end %-- In / Out loop (j)

clearvars tmp* j k
%-- no longer need separate arrays for tracks in and out (from 11)
clearvars data_sPos_Tracks_*

disp(['[' datestr(now,31),']     ...done'])

%%
%---------------------------------------------------------------
% -- (X) Save out workspace variables
%---------------------------------------------------------------
disp(['[' datestr(now,31),'] Saving workspace variables']);

%-- set the warning state back to original
warning(w);
clearvars w;

%-- Save the workspace variables using the base path and filename
%-- CATCH AN IFEXIST by appending a date/Time stamp
save([inFileDir strrep(inFile,'.csv','_processed.mat')]);
disp(['[' datestr(now,31),']      saved: ',inFileDir strrep(inFile,'.csv','_processed.mat')]);
disp(['[' datestr(now,31),']     ...done'])

%-- Let the user know we're done
disp(['[' datestr(now,31),'] ---------/ ',mfilename,' ----------']);
%disp(['[' datestr(now,31),'] ---------- Finished ----------']);