This repository contains code for post-processing spheroid imaging and tracking data acquired on a lightsheet/SPIM microscope.

# PREPROCESSING: Downsampling Lightsheet data #
### Preamble ###
This IJ1 script is used to reduce the size of CZI lightsheet data from a Zeiss Lightsheet Z.1. Given the limited options on the Z.1 for decreasing the size of the acquired data, you may want to do this to speed up further analysis (eg for tracking).

To save having to load the whole dataset into memory at once, the script downsamples one timepoint at a time so your computer will have to be able to hold one timepoint worth of data in memory.

This script is referenced in a book chapter called ["Live Imaging of Cell Invasion Using a Multicellular Spheroid Model and Light-Sheet Microscopy."](https://doi.org/10.1007/978-3-319-67358-5_11).

### Usage ###

* Download and update [Fiji](http://fiji.sc)
* Open the script `PreProcessing/LSFM_downsample.ijm` in the Fiji Script Editor and hit run, you'll be asked for an input file then be offered options to downsample including:
    * Binning (ie reducing pixel number)
	* Decreasing Bit Depth from 16 -> 8 bit
	* Cropping the field
	* Removing unwanted channels
* Your output will be saved in a created subfolder called processed (not overwritten if it already exists) 

### Output ###
Files are saved as one tif-stack per timepoint. These data are suitable for tracking in Fiji or can be imported for example into Imaris (tested on version 8.2 and higher), or Fiji using `[File > Import > Image Sequence]`.

# TRACKANALYSIS: Post-processing spheroid cell tracking data #
### Preamble ###
The most basic tracking outputs from any software are 
1) position (Cartesian coordinates in X, Y and Z)
2) A time stamp (calibrated time or frame number)
3) A Track ID to link features across frames. 

These are available from Imaris as the 'Position' spot statistics or Trackmate as the 'Spots in tracks statistics'. Either should be saved as a CSV file. You will also need to know the position of the spheroid centre. Be sure to use the same software you used to track the data to measure the position of the spheroid as origin locations are software dependent!

### Usage ###
The processing has two parts. The first, `s01_processData.m` takes the outputs from either Imaris or Trackmate and calculates a range of parameters by which to characterise the spots and tracks. This includes speed, straightness and radial components of velocity. The second part `s02_plotFigures.m` is used to take the output of the processing script and present the data as easy to understand and compare plots.

* Open MATLAB 2017b or newer
* Run the processing script (`s01_processData.m`). You will be asked for some extra information including the position of the centroid, data source and temporal calibration. Hit OK to run. Progress is logged to the console.
* Once this has completed, you can run the second file `s02_plotFigures.m` which will request as an input, the output file from the processing script (see "outputs" below).
* Before you do this, open the script and pay attention to section 2 of the code which details a number of output options:

~~~
plotOpts.whichTracks=0;       %-- [0/1/2] Whether to plot:
                                % [0] all tracks
                                % [1] only INNIES (no feature goes beyond boundary+20)
                                % [2] only OUTIES (any single feature goes beyond boundary+20)
plotOpts.bigSpheroids=0;     %-- [0/1] Determines how the SpotSpeed/SpotStraightness by Distance bins are calculated. 
                                % [0]= Use 2 bins for distance graphs (in/out)
                                % [1]= Use 6 bins for distance (3 in / 3 out)
plotOpts.normMax=0;          %-- [0/1] How to normalise the graphs of parameter by time/distance
                                % [0]= Normalise by area under curve
								% [1]= Normalise by maximum value of 'Y'
plotOpts.minSpots=1000;      %-- Need at least this many Spots to plot a line graph - prevents crappy-looking normalised histograms
plotOpts.saveFIG=0;          %-- [0/1] Save a matlab FIG file? (NOTE: if neither saveFIG nor savePNG are used, graphs will stay open)
plotOpts.savePNG=0;          %-- [0/1] Save a PNG file? (see note above)
plotOpts.saveTracks=0;       %-- [0/1] Save plots of radial distance over time for migrant tracks? WARNING: can produce a lot of files!
plotOpts.addGraphTitle=1;    %-- [0/1] Add a title to the graphs (IE "Spot Speed by Radial Distance" &c.)
plotOpts.addFileNameTitle=0; %-- [0/1] Do you want to add a FileName title to each plot?
                                % Recommended to TURN OFF for fig files as it changes the axis layout
~~~
* Note that you can run each section separately if you only want a subset of the plots, just make sure that the first two sections have been run first (input & Settings).

### Output: ###

During the processing stage, the MATLAB array `data_sPos` will be created upon import (first 5 columns) will have one row per spot and the following columns:

    COLUMN      CONTENT
    1           x-position (um)
    2           y-position (um)
    3           z-position (um)
    4           frame number
    5           Track ID
    6           Spot Azimuth
    7           Spot Elevation
    8           Spot Radial distance (um)
    9           Step displacements (forward) (um)
    10          Step Speed (forward) (um/s)
    11          Windowed Speed (forward) (um/s)
    12          Windowed Straightness (forward) (um/s)
    13          x-displacement (um)
    14          y-displacement (um)
    15          z-displacement (um)
    16          x-component of speed (um/s)
    17          y-component of speed (um/s)
    18          z-component of speed (um/s)
    19          azimuth angle (from origin) in radians
    20          elevation angle (from origin) in radians
    21          radial dist (um)
    22          logitudinal speed (azimuthal) (um/s)
    23          lateral speed (elevatorial) (um/s)
    24          radial speed (um/s)

In section 14 of the processing code, two further output arrays are created called `data_TrackDetails_{In|Out}`. These record track-level data (one row per track) broken down into segments of continuous time spent inside or outside the boundary of the spheroid. `data_TrackDetails_In` contains only tracks that remain within the spheroid boundary. `data_TrackDetails_Out` records tracks that have any part of the track outside the boundary. Note that this last case includes tracks that are _never_ inside the boundary.

    COLUMN	CONTENT	DESCRIPTION
    1	TrackID             10^9 padded
    2	totalPoints         Number of features in track
    3	inPoints            Features inside spheroid
    4	outPoints           Features outside spheroid
    5	azVelIn             Mean 1-step lateral velocity of inside parts
    6	azVelOut            Mean 1-step lateral velocity of outside parts
    7	elVelIn             Mean 1-step longitudinal velocity of inside parts
    8	elVelOut            Mean 1-step longitudinal velocity of outside parts
    9	radVelIn            Mean 1-step radial velocity of inside parts
    10	radVelOut          Mean 1-step radial velocity of outside parts
    11	segments inside    Number of contiguous inside segments
    12	segments outside   Number of contiguous outside segments
    13	straightIn         Mean straightness of inside segments
    14	straightOut        Mean straightness of outside segments
    15	speedIn            Mean speed of inside segments
    16	speedOut           Mean speed of outside segments
    17	winStraightIn      Mean windowed straightness Inside segs
    18	winStraightOut     Mean windowed straightness Outside segs

At the end of the processing a file will be created in the same directory as the input CSV named `{INPUT_FILENAME}_processed.mat`

The plotting script will produce over 20 plots for each dataset depending upon your settings and they will be placed into a folder of the same name as the MAT file used to create them (minus extension).
	
# Acknowledgements and Licence #

* The code was written and tested by [Dave Mason](http://pcwww.liv.ac.uk/~dnmason), from the University of Liverpool [Centre for Cell Imaging](http://cci.liv.ac.uk).
* Discussions with Rapha&euml;l L&eacute;vy and Rachel Bearon were invaluable to the progress of the project. Violaine S&eacute;e and Rosalie Richards were also heavily involved in the direction of the project and collection of lightsheet datasets.
* The code here is provided under an [MIT Licence](https://en.wikipedia.org/wiki/MIT_License) which allows use and modification for any purpose as long as the original source is attributed via the included LICENCE file.
